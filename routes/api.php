<?php

use Illuminate\Support\Facades\Redis;

Route::get('/', function () {
    $data = [
        'event' => 'UserSignedUp',
        'data' => [
            'username' => 'JohnDoe'
        ]
    ];

    event(new \App\Events\ClientNotification('new clent has been added'));
    \Illuminate\Support\Facades\Log::info(event(new \App\Events\LeadEventNotification('hello')));
    // In Episode 4, we'll use Laravel's event broadcasting.
//    Redis::publish('test-channel', json_encode($data));

    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'bMK38z7WFfXR', 'namespace' => 'Api'], function () {
    // auth routes
    Route::group(['prefix' => 'auth'], function () {
        Route::get('check', [
            'uses' => 'AuthController@CheckLogin',
        ]);
        Route::post('login', [
            'uses' => 'AuthController@login',
        ]);
        Route::get('logout', [
            'uses' => 'AuthController@logout',
        ]);
    });
    Route::group(['middleware' => 'auth.api'], function () {
        // level 2 routes
        Route::group(['prefix' => 'level1', 'namespace' => 'Level1'], function () {
            // company routes
            Route::group(['prefix' => 'company'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:company_list'],
                    'uses' => 'CompanyController@CompaniesList',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:company_add'],
                    'uses' => 'CompanyController@store',
                ]);
                Route::get('info/{company}', [
                    'middleware' => ['roles.permissions:company_view'],
                    'uses' => 'CompanyController@CompanyInfo',
                ]);
                Route::post('update', [
                    'middleware' => ['roles.permissions:company_update'],
                    'uses' => 'CompanyController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:company_delete'],
                    'uses' => 'CompanyController@Destroy',
                ]);
            });
            // product routes
            Route::group(['prefix' => 'product'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:product_list'],
                    'uses' => 'ProductController@ProductsList',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:product_add'],
                    'uses' => 'ProductController@store',
                ]);
                Route::get('view/{product_id}', [
                    'middleware' => ['roles.permissions:product_view'],
                    'uses' => 'ProductController@View',
                ]);
                Route::get('info/{product}', [
                    'middleware' => ['roles.permissions:product_update'],
                    'uses' => 'ProductController@ProductInfo',
                ]);
                Route::put('update', [
                    'middleware' => ['roles.permissions:product_update'],
                    'uses' => 'ProductController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:product_delete'],
                    'uses' => 'ProductController@Destroy',
                ]);
            });
            // feature routes
            Route::group(['prefix' => 'feature'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:feature_list'],
                    'uses' => 'FeatureController@FeaturesList',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:feature_add'],
                    'uses' => 'FeatureController@store',
                ]);
                Route::get('info/{feature}', [
                    'middleware' => ['roles.permissions:feature_view'],
                    'uses' => 'FeatureController@FeatureInfo',
                ]);
                Route::put('update', [
                    'middleware' => ['roles.permissions:feature_update'],
                    'uses' => 'FeatureController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:feature_delete'],
                    'uses' => 'FeatureController@Destroy',
                ]);
            });
            // package routes
            Route::group(['prefix' => 'package'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:package_list'],
                    'uses' => 'PackageController@PackageList',
                ]);
                Route::get('get-add', [
                    'middleware' => ['roles.permissions:package_add'],
                    'uses' => 'PackageController@GetAdd',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:package_add'],
                    'uses' => 'PackageController@store',
                ]);
                Route::get('info/{package_id}', [
                    'middleware' => ['roles.permissions:package_view'],
                    'uses' => 'PackageController@PackageInfo',
                ]);
                Route::get('get-update/{package_id}', [
                    'middleware' => ['roles.permissions:package_update'],
                    'uses' => 'PackageController@GetUpdate',
                ]);
                Route::put('update', [
                    'middleware' => ['roles.permissions:package_update'],
                    'uses' => 'PackageController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:package_delete'],
                    'uses' => 'PackageController@Destroy',
                ]);
            });
            // package routes
            Route::group(['prefix' => 'user'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:user_list'],
                    'uses' => 'UserController@UserList',
                ]);
                Route::get('get-add', [
                    'middleware' => ['roles.permissions:user_add'],
                    'uses' => 'UserController@GetAdd',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:user_add'],
                    'uses' => 'UserController@store',
                ]);
                Route::get('info/{user_id}', [
                    'middleware' => ['roles.permissions:user_view'],
                    'uses' => 'UserController@UserInfo',
                ]);
                Route::get('get-update/{user_id}', [
                    'middleware' => ['roles.permissions:user_update'],
                    'uses' => 'UserController@GetUpdate',
                ]);
                Route::put('update', [
                    'middleware' => ['roles.permissions:user_update'],
                    'uses' => 'UserController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:user_delete'],
                    'uses' => 'UserController@Destroy',
                ]);
            });
            // role routes
            Route::group(['prefix' => 'role'], function () {
                Route::get('list', [
                    'middleware' => ['roles.permissions:role_list'],
                    'uses' => 'RolePermissionController@RoleList',
                ]);
                Route::get('get-add', [
                    'middleware' => ['roles.permissions:role_add'],
                    'uses' => 'RolePermissionController@GetAdd',
                ]);
                Route::post('add', [
                    'middleware' => ['roles.permissions:role_add'],
                    'uses' => 'RolePermissionController@store',
                ]);
                Route::get('info/{role_id}', [
                    'middleware' => ['roles.permissions:role_view'],
                    'uses' => 'RolePermissionController@RoleInfo',
                ]);
                Route::get('get-update/{role_id}', [
                    'middleware' => ['roles.permissions:role_update'],
                    'uses' => 'RolePermissionController@GetUpdate',
                ]);
                Route::put('update', [
                    'middleware' => ['roles.permissions:role_update'],
                    'uses' => 'RolePermissionController@PostUpdate',
                ]);
                Route::delete('destroy/{id}', [
                    'middleware' => ['roles.permissions:role_delete'],
                    'uses' => 'RolePermissionController@Destroy',
                ]);
            });
        });
        // level 2 routes
        Route::group(['prefix' => 'level2', 'namespace' => 'Level2'], function () {
            //manager panel routes
            route::group(['prefix' => 'hod', 'namespace' => 'Hod'], function () {
                //sale panel routes
                route::group(['prefix' => 'sale', 'namespace' => 'Sale'], function () {
                    // header route
                    Route::group(['prefix' => 'header'], function () {
                        Route::get('info', [
                            'uses' => 'DashboardController@GetHeaderInfo',
                        ]);
                    });
                    Route::group(['prefix' => 'dashboard'], function () {
                        Route::get('info', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_index_view'],
                            'uses' => 'DashboardController@GetInfo',
                        ]);
                    });
                    // profile routes
                    Route::group(['prefix' => 'user'], function () {
                        Route::get('info', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_user_profile_view'],
                            'uses' => 'UserController@GetInfo',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_user_profile_update'],
                            'uses' => 'UserController@PostUpdate',
                        ]);
                    });
                    // client routes
                    Route::group(['prefix' => 'client'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_list'],
                            'uses' => 'ClientController@ClientList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_add'],
                            'uses' => 'ClientController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_add'],
                            'uses' => 'ClientController@store',
                        ]);
                        Route::get('info/{client_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_view'],
                            'uses' => 'ClientController@ClientInfo',
                        ]);
                        Route::get('get-update/{client_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_update'],
                            'uses' => 'ClientController@GetUpdate',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_update'],
                            'uses' => 'ClientController@PostUpdate',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_update'],
                            'uses' => 'ClientController@AssignAgent',
                        ]);
                        Route::delete('destroy/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_client_delete'],
                            'uses' => 'ClientController@Destroy',
                        ]);
                        Route::delete('remove-agent/{id}/{customer_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_agent_delete'],
                            'uses' => 'ClientController@RemoveAgent',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_sale_client_followup_add'],
                                'uses' => 'ClientFollowUpController@store',
                            ]);
                        });
                    });
                    // proposal routes
                    Route::group(['prefix' => 'proposal'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_proposal_list'],
                            'uses' => 'ProposalController@ProposalList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_proposal_add'],
                            'uses' => 'ProposalController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_proposal_add'],
                            'uses' => 'ProposalController@store',
                        ]);
                        Route::get('info/{proposal_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_proposal_view'],
                            'uses' => 'ProposalController@ProposalInfo',
                        ]);
                        Route::delete('destroy/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_proposal_delete'],
                            'uses' => 'ProposalController@Destroy',
                        ]);
                    });
                    // lead routes
                    Route::group(['prefix' => 'lead'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_list'],
                            'uses' => 'LeadController@LeadList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_add'],
                            'uses' => 'LeadController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_add'],
                            'uses' => 'LeadController@store',
                        ]);
                        Route::get('info/{lead_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_view'],
                            'uses' => 'LeadController@LeadInfo',
                        ]);
                        Route::put('status-update', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_update'],
                            'uses' => 'LeadController@PutStatusUpdate',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_lead_update'],
                            'uses' => 'LeadController@AssignAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_agent_delete'],
                            'uses' => 'LeadController@RemoveAgent',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_sale_lead_followup_add'],
                                'uses' => 'LeadFollowUpController@store',
                            ]);
                        });
                    });
                    // sale routes
                    Route::group(['prefix' => 'sale'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_sale_list'],
                            'uses' => 'SaleController@SaleList',
                        ]);
                        Route::get('info/{sale_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_sale_view'],
                            'uses' => 'SaleController@SaleInfo',
                        ]);
                    });
                    // report routes
                    Route::group(['prefix' => 'report'], function () {
                        Route::post('info', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_report_view'],
                            'uses' => 'ReportController@Reports',
                        ]);
                    });
                    // agent routes
                    Route::group(['prefix' => 'agent'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_agent_list'],
                            'uses' => 'AgentsController@AgentList',
                        ]);
                        Route::get('info/{user_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_agent_view'],
                            'uses' => 'AgentsController@AgentInfo',
                        ]);
                    });
                    // ticket routes
                    Route::group(['prefix' => 'ticket'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_list'],
                            'uses' => 'TicketsController@TicketList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_add'],
                            'uses' => 'TicketsController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_add'],
                            'uses' => 'TicketsController@store',
                        ]);
                        Route::get('info/{ticket_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_view'],
                            'uses' => 'TicketsController@TicketInfo',
                        ]);
                        Route::put('priority-update', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_priority_update'],
                            'uses' => 'TicketsController@UpdatePriority',
                        ]);
                        Route::put('status-update', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_status_update'],
                            'uses' => 'TicketsController@UpdateStatus',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_update'],
                            'uses' => 'TicketsController@AssignToAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_sale_ticket_update'],
                            'uses' => 'TicketsController@RemoveAgent',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_sale_ticket_followup_add'],
                                'uses' => 'TicketFollowUpController@store',
                            ]);
                        });
                    });
                });
                // crm routes
                Route::group(['prefix' => 'crm', 'namespace' => 'CRM'], function () {
                    // header route
                    Route::group(['prefix' => 'header'], function () {
                        Route::get('info', [
                            'uses' => 'DashboardController@GetHeaderInfo',
                        ]);
                    });
                    Route::group(['prefix' => 'dashboard'], function () {
                        Route::get('info', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_index_view'],
                            'uses' => 'DashboardController@GetInfo',
                        ]);
                    });
                    // profile routes
                    Route::group(['prefix' => 'user'], function () {
                        Route::get('info', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_user_profile_view'],
                            'uses' => 'UserController@GetInfo',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_user_profile_update'],
                            'uses' => 'UserController@PostUpdate',
                        ]);
                    });
                    // client routes
                    Route::group(['prefix' => 'client'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_client_list'],
                            'uses' => 'ClientController@ClientList',
                        ]);
                        Route::get('info/{client_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_client_view'],
                            'uses' => 'ClientController@ClientInfo',
                        ]);
                        Route::get('get-update/{opt_id}/{client_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_approved_client_update'],
                            'uses' => 'ClientController@GetUpdate',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_approved_client_update'],
                            'uses' => 'ClientController@PostUpdate',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_client_followup_add'],
                                'uses' => 'ClientFollowUpController@store',
                            ]);
                        });
                        // emergency contact routes
                        Route::group(['prefix' => 'emergency_contact'], function () {
                            Route::get('get-update/{opt_id}/{client_id}', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_approved_emergency_contact_update'],
                                'uses' => 'ClientController@GetECUpdate',
                            ]);
                            Route::put('update', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_approved_emergency_contact_update'],
                                'uses' => 'ClientController@PostECUpdate',
                            ]);
                        });
                        // vehicle routes
                        Route::group(['prefix' => 'vehicle'], function () {
                            Route::get('get-update/{opt_id}/{vehicle_id}', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_approved_vehicle_update'],
                                'uses' => 'ClientController@GetVEUpdate',
                            ]);
                            Route::put('update', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_approved_vehicle_update'],
                                'uses' => 'ClientController@PostVEUpdate',
                            ]);
                        });
                    });
                    // lead routes
                    Route::group(['prefix' => 'lead'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_list'],
                            'uses' => 'LeadController@LeadList',
                        ]);
                        Route::get('get-update/{lead_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_update'],
                            'uses' => 'LeadController@LeadInfo',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_update'],
                            'uses' => 'LeadController@PostUpdate',
                        ]);
                        Route::get('get-agent/{lead_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_assign_agent_add'],
                            'uses' => 'LeadController@GetAgents',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_assign_agent_add'],
                            'uses' => 'LeadController@AssignAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_lead_assign_agent_delete'],
                            'uses' => 'LeadController@RemoveAgent',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_lead_followup_add'],
                                'uses' => 'LeadFollowUpController@store',
                            ]);
                        });
                    });
                    // ticket routes
                    Route::group(['prefix' => 'ticket'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_list'],
                            'uses' => 'TicketsController@TicketList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_add'],
                            'uses' => 'TicketsController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_add'],
                            'uses' => 'TicketsController@store',
                        ]);
                        Route::get('info/{ticket_id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_view'],
                            'uses' => 'TicketsController@TicketInfo',
                        ]);
                        Route::put('priority-update', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_update'],
                            'uses' => 'TicketsController@UpdatePriority',
                        ]);
                        Route::put('status-update', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_update'],
                            'uses' => 'TicketsController@UpdateStatus',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_update'],
                            'uses' => 'TicketsController@AssignToAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-2_hod_crm_ticket_update'],
                            'uses' => 'TicketsController@RemoveAgent',
                        ]);
                        // follow up routes
                        Route::group(['prefix' => 'followup'], function () {
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_ticket_followup_add'],
                                'uses' => 'TicketFollowUpController@store',
                            ]);
                        });
                        // operational ticket routes
                        Route::group(['prefix' => 'operational'], function () {
                            Route::get('list', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_list'],
                                'uses' => 'OperationalTicketsController@TicketList',
                            ]);
                            Route::get('get-add', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_add'],
                                'uses' => 'OperationalTicketsController@GetAdd',
                            ]);
                            Route::post('add', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_add'],
                                'uses' => 'OperationalTicketsController@store',
                            ]);
                            Route::get('info/{ticket_id}', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_view'],
                                'uses' => 'OperationalTicketsController@TicketInfo',
                            ]);
                            Route::put('priority-update', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_update'],
                                'uses' => 'OperationalTicketsController@UpdatePriority',
                            ]);
                            Route::put('status-update', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_update'],
                                'uses' => 'OperationalTicketsController@UpdateStatus',
                            ]);
                            Route::put('assign-agent', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_update'],
                                'uses' => 'OperationalTicketsController@AssignToAgent',
                            ]);
                            Route::delete('remove-agent/{id}', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_operational_ticket_update'],
                                'uses' => 'OperationalTicketsController@RemoveAgent',
                            ]);
                        });
                        // agent routes
                        Route::group(['prefix' => 'agent'], function () {
                            Route::get('list', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_agent_list'],
                                'uses' => 'AgentsController@AgentList',
                            ]);
                            Route::get('info/{user_id}', [
                                'middleware' => ['roles.permissions:level-2_hod_crm_agent_view'],
                                'uses' => 'AgentsController@AgentInfo',
                            ]);
                        });
                    });
                });
            });
        });
        // level 3 routes
        Route::group(['prefix' => 'level3', 'namespace' => 'Level3'], function () {
            // sale routes
            Route::group(['prefix' => 'sale', 'namespace' => 'SALE'], function () {
                // header route
                Route::group(['prefix' => 'header'], function () {
                    Route::get('info', [
                        'uses' => 'DashboardController@GetHeaderInfo',
                    ]);
                });
                Route::group(['prefix' => 'dashboard'], function () {
                    Route::get('info', [
                        'middleware' => ['roles.permissions:level-3_sale_index_view'],
                        'uses' => 'DashboardController@GetInfo',
                    ]);
                });
                // profile routes
                Route::group(['prefix' => 'user'], function () {
                    Route::get('info', [
                        'middleware' => ['roles.permissions:level-3_sale_user_profile_view'],
                        'uses' => 'UserController@GetInfo',
                    ]);
                    Route::put('update', [
                        'middleware' => ['roles.permissions:level-3_sale_user_profile_update'],
                        'uses' => 'UserController@PostUpdate',
                    ]);
                });
                // client routes
                Route::group(['prefix' => 'client'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_sale_client_list'],
                        'uses' => 'ClientController@ClientList',
                    ]);
                    Route::get('get-add', [
                        'middleware' => ['roles.permissions:level-3_sale_client_add'],
                        'uses' => 'ClientController@GetAdd',
                    ]);
                    Route::post('add', [
                        'middleware' => ['roles.permissions:level-3_sale_client_add'],
                        'uses' => 'ClientController@store',
                    ]);
                    Route::get('info/{client_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_client_view'],
                        'uses' => 'ClientController@ClientInfo',
                    ]);
                    Route::get('get-update/{client_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_client_update'],
                        'uses' => 'ClientController@GetUpdate',
                    ]);
                    Route::put('update', [
                        'middleware' => ['roles.permissions:level-3_sale_client_update'],
                        'uses' => 'ClientController@PostUpdate',
                    ]);
                    Route::delete('destroy/{id}', [
                        'middleware' => ['roles.permissions:level-3_sale_client_delete'],
                        'uses' => 'ClientController@Destroy',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_sale_client_followup_add'],
                            'uses' => 'ClientFollowUpController@store',
                        ]);
                    });
                });
                // proposal routes
                Route::group(['prefix' => 'proposal'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_sale_proposal_list'],
                        'uses' => 'ProposalController@ProposalList',
                    ]);
                    Route::get('get-add', [
                        'middleware' => ['roles.permissions:level-3_sale_proposal_add'],
                        'uses' => 'ProposalController@GetAdd',
                    ]);
                    Route::post('add', [
                        'middleware' => ['roles.permissions:level-3_sale_proposal_add'],
                        'uses' => 'ProposalController@store',
                    ]);
                    Route::get('info/{proposal_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_proposal_view'],
                        'uses' => 'ProposalController@ProposalInfo',
                    ]);
                    Route::delete('destroy/{id}', [
                        'middleware' => ['roles.permissions:level-3_sale_proposal_delete'],
                        'uses' => 'ProposalController@Destroy',
                    ]);
                });
                // lead routes
                Route::group(['prefix' => 'lead'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_sale_lead_list'],
                        'uses' => 'LeadController@LeadList',
                    ]);
                    Route::get('get-add', [
                        'middleware' => ['roles.permissions:level-3_sale_lead_add'],
                        'uses' => 'LeadController@GetAdd',
                    ]);
                    Route::post('add', [
                        'middleware' => ['roles.permissions:level-3_sale_lead_add'],
                        'uses' => 'LeadController@store',
                    ]);
                    Route::get('info/{lead_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_lead_view'],
                        'uses' => 'LeadController@LeadInfo',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_sale_lead_followup_add'],
                            'uses' => 'LeadFollowUpController@store',
                        ]);
                    });
                });
                // sale routes
                Route::group(['prefix' => 'sale'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_sale_sale_list'],
                        'uses' => 'SaleController@SaleList',
                    ]);
                    Route::get('info/{sale_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_sale_view'],
                        'uses' => 'SaleController@SaleInfo',
                    ]);
                });
                // report routes
                Route::group(['prefix' => 'report'], function () {
                    Route::post('info', [
                        'middleware' => ['roles.permissions:level-3_sale_report_view'],
                        'uses' => 'ReportController@Reports',
                    ]);
                });
                // ticket routes
                Route::group(['prefix' => 'ticket'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_list'],
                        'uses' => 'TicketsController@TicketList',
                    ]);
                    Route::get('get-add', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_add'],
                        'uses' => 'TicketsController@GetAdd',
                    ]);
                    Route::post('add', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_add'],
                        'uses' => 'TicketsController@store',
                    ]);
                    Route::get('info/{ticket_id}', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_view'],
                        'uses' => 'TicketsController@TicketInfo',
                    ]);
                    Route::put('priority-update', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_update'],
                        'uses' => 'TicketsController@UpdatePriority',
                    ]);
                    Route::put('status-update', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_update'],
                        'uses' => 'TicketsController@UpdateStatus',
                    ]);
                    Route::put('assign-agent', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_update'],
                        'uses' => 'TicketsController@AssignToAgent',
                    ]);
                    Route::delete('remove-agent/{id}', [
                        'middleware' => ['roles.permissions:level-3_sale_ticket_update'],
                        'uses' => 'TicketsController@RemoveAgent',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_sale_ticket_followup_add'],
                            'uses' => 'TicketFollowUpController@store',
                        ]);
                    });
                    // operational ticket routes
                    Route::group(['prefix' => 'operational'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_ticket_list'],
                            'uses' => 'OperationalTicketsController@TicketList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_add'],
                            'uses' => 'OperationalTicketsController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_add'],
                            'uses' => 'OperationalTicketsController@store',
                        ]);
                        Route::get('info/{ticket_id}', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_view'],
                            'uses' => 'OperationalTicketsController@TicketInfo',
                        ]);
                        Route::put('priority-update', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_update'],
                            'uses' => 'OperationalTicketsController@UpdatePriority',
                        ]);
                        Route::put('status-update', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_update'],
                            'uses' => 'OperationalTicketsController@UpdateStatus',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_update'],
                            'uses' => 'OperationalTicketsController@AssignToAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-3_sale_operational_update'],
                            'uses' => 'OperationalTicketsController@RemoveAgent',
                        ]);
                    });
                });
            });
            // crm routes
            Route::group(['prefix' => 'crm', 'namespace' => 'CRM'], function () {
                // header route
                Route::group(['prefix' => 'header'], function () {
                    Route::get('info', [
                        'uses' => 'DashboardController@GetHeaderInfo',
                    ]);
                });
                Route::group(['prefix' => 'dashboard'], function () {
                    Route::get('info', [
                        'middleware' => ['roles.permissions:level-3_crm_index_view'],
                        'uses' => 'DashboardController@GetInfo',
                    ]);
                });
                // profile routes
                Route::group(['prefix' => 'user'], function () {
                    Route::get('info', [
                        'middleware' => ['roles.permissions:level-3_crm_user_profile_view'],
                        'uses' => 'UserController@GetInfo',
                    ]);
                    Route::put('update', [
                        'middleware' => ['roles.permissions:level-3_crm_user_profile_update'],
                        'uses' => 'UserController@PostUpdate',
                    ]);
                });
                // client routes
                Route::group(['prefix' => 'client'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_crm_client_list'],
                        'uses' => 'ClientController@ClientList',
                    ]);
                    Route::get('info/{client_id}', [
                        'middleware' => ['roles.permissions:level-3_crm_client_view'],
                        'uses' => 'ClientController@ClientInfo',
                    ]);
                    Route::get('get-update/{opt_id}/{client_id}', [
                        'middleware' => ['roles.permissions:level-3_crm_approved_update'],
                        'uses' => 'ClientController@GetUpdate',
                    ]);
                    Route::put('update', [
                        'middleware' => ['roles.permissions:level-3_crm_approved_update'],
                        'uses' => 'ClientController@PostUpdate',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_crm_client_followup_add'],
                            'uses' => 'ClientFollowUpController@store',
                        ]);
                    });
                    // emergency contact routes
                    Route::group(['prefix' => 'emergency_contact'], function () {
                        Route::get('get-update/{opt_id}/{client_id}', [
                            'middleware' => ['roles.permissions:level-3_crm_approved_emergency_contact_update'],
                            'uses' => 'ClientController@GetECUpdate',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-3_crm_approved_emergency_contact_update'],
                            'uses' => 'ClientController@PostECUpdate',
                        ]);
                    });
                    // vehicle routes
                    Route::group(['prefix' => 'vehicle'], function () {
                        Route::get('get-update/{opt_id}/{vehicle_id}', [
                            'middleware' => ['roles.permissions:level-3_crm_approved_vehicle_update'],
                            'uses' => 'ClientController@GetVEUpdate',
                        ]);
                        Route::put('update', [
                            'middleware' => ['roles.permissions:level-3_crm_approved_vehicle_update'],
                            'uses' => 'ClientController@PostVEUpdate',
                        ]);
                    });
                });
                // lead routes
                Route::group(['prefix' => 'lead'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_crm_lead_list'],
                        'uses' => 'LeadController@LeadList',
                    ]);
                    Route::get('get-update/{lead_id}', [
                        'middleware' => ['roles.permissions:level-3_crm_lead_view'],
                        'uses' => 'LeadController@LeadInfo',
                    ]);
                    Route::put('update', [
                        'middleware' => ['roles.permissions:client_update'],
                        'uses' => 'LeadController@PostUpdate',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_crm_lead_followup_add'],
                            'uses' => 'LeadFollowUpController@store',
                        ]);
                    });
                });
                // ticket routes
                Route::group(['prefix' => 'ticket'], function () {
                    Route::get('list', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_list'],
                        'uses' => 'TicketsController@TicketList',
                    ]);
                    Route::get('get-add', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_add'],
                        'uses' => 'TicketsController@GetAdd',
                    ]);
                    Route::post('add', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_add'],
                        'uses' => 'TicketsController@store',
                    ]);
                    Route::get('info/{ticket_id}', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_view'],
                        'uses' => 'TicketsController@TicketInfo',
                    ]);
                    Route::put('priority-update', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_update'],
                        'uses' => 'TicketsController@UpdatePriority',
                    ]);
                    Route::put('status-update', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_update'],
                        'uses' => 'TicketsController@UpdateStatus',
                    ]);
                    Route::put('assign-agent', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_update'],
                        'uses' => 'TicketsController@AssignToAgent',
                    ]);
                    Route::delete('remove-agent/{id}', [
                        'middleware' => ['roles.permissions:level-3_crm_ticket_update'],
                        'uses' => 'TicketsController@RemoveAgent',
                    ]);
                    // follow up routes
                    Route::group(['prefix' => 'followup'], function () {
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_crm_ticket_followup_add'],
                            'uses' => 'TicketFollowUpController@store',
                        ]);
                    });
                    // operational ticket routes
                    Route::group(['prefix' => 'operational'], function () {
                        Route::get('list', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_list'],
                            'uses' => 'OperationalTicketsController@TicketList',
                        ]);
                        Route::get('get-add', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_add'],
                            'uses' => 'OperationalTicketsController@GetAdd',
                        ]);
                        Route::post('add', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_add'],
                            'uses' => 'OperationalTicketsController@store',
                        ]);
                        Route::get('info/{ticket_id}', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_view'],
                            'uses' => 'OperationalTicketsController@TicketInfo',
                        ]);
                        Route::put('priority-update', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_update'],
                            'uses' => 'OperationalTicketsController@UpdatePriority',
                        ]);
                        Route::put('status-update', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_update'],
                            'uses' => 'OperationalTicketsController@UpdateStatus',
                        ]);
                        Route::put('assign-agent', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_update'],
                            'uses' => 'OperationalTicketsController@AssignToAgent',
                        ]);
                        Route::delete('remove-agent/{id}', [
                            'middleware' => ['roles.permissions:level-3_crm_operational_ticket_update'],
                            'uses' => 'OperationalTicketsController@RemoveAgent',
                        ]);
                    });
                });
            });
        });
    });
});
