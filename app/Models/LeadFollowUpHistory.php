<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadFollowUpHistory extends Model
{
    use SoftDeletes;

    protected $table = 'tb_lead_followup_history';
    protected $fillable = [
        'customer_id',
        'lead_id',
        'user_id',
        'comment',
        'commented_by',
        'is_edit'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
