<?php

namespace App\Models;

use App\HelperModules\DateTimeModule;
use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leads extends Model
{
    use SoftDeletes;

    protected $table = 'tb_leads';
    protected $primaryKey = 'lead_id';
    public $incrementing = false;
    protected $fillable = [
        'lead_id',
        'proposal_id',
        'user_id',
        'status'
    ];

    /**
     * @param $value
     */
    public function setLeadIdAttribute($value)
    {
        $this->attributes['lead_id'] = HelperModule::CustomID('LD');
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return DateTimeModule::TimeFormat($value, 'd/m/Y');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicLeadInfo($select = '*')
    {
        return self::select($select)->orderBy('lead_id', 'desc');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllLeads($select = '*')
    {
        return self::BasicLeadInfo($select)->with('LeadProposal.ClientInfo');
    }

    /**
     * @param string $select
     * @param string $user_select
     * @return mixed
     */
    static public function AllLeadsWithDetail($select = '*', $user_select = '*')
    {
        return self::AllLeads($select)
            ->with(['LeadFollowUpHistory.UserInfo' => function ($query) use ($user_select) {
                $query->select($user_select);
            }]);
    }

    /**
     * @param $user_id
     * @param string $select
     * @return mixed
     */
    static public function GetLeadsByUserId($user_id, $select = '*')
    {
        return self::BasicLeadInfo($select)
            ->whereHas('Agents', function ($query) use ($user_id) {
                $query->where('assigned_to', $user_id);
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function LeadStatusHistory()
    {
        return $this->hasMany(LeadStatusHistory::class, 'lead_id', 'lead_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function LeadProposal()
    {
        return $this->belongsTo(Proposal::class, 'proposal_id', 'proposal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function LeadFollowUpHistory()
    {
        return $this->hasMany(LeadFollowUpHistory::class, 'lead_id', 'lead_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function Agents()
    {
        return $this->hasMany(LeadAgents::class, 'lead_id', 'lead_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function LeadVehicle()
    {
        return $this->hasMany(CustomerVehicleLeadHistory::class, 'lead_id', 'lead_id');
    }
}
