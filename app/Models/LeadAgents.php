<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadAgents extends Model
{
    use SoftDeletes;

    protected $table = 'tb_lead_agents';
    protected $primaryKey = 'lead_agent_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lead_id',
        'assigned_by',
        'assigned_to'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AssignedBy()
    {
        return $this->belongsTo(User::class, 'assigned_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AssignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to', 'user_id');
    }
}
