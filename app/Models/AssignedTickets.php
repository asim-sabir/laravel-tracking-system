<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedTickets extends Model
{
    use SoftDeletes;

    protected $table = 'tb_assigned_tickets';
    protected $fillable = [
        'ticket_id',
        'assigned_to',
        'assigned_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AssignedUserInfo()
    {
        return $this->belongsTo(User::class, 'assigned_to', 'user_id');
    }
}
