<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketStatusHistory extends Model
{

    protected $table = 'tb_ticket_status_history';
    protected $primaryKey = 'id';
    protected $fillable = [
        'ticket_id',
        'user_id',
        'priority_level',
        'status'
    ];
}
