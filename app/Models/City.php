<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $table = 'cities';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $dates = ['created_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_code',
        'region_id',
        'name',
        'slug',
        'is_tcs',
        'is_leopards'
    ];

    /**
     * @return mixed
     */
    static public function AllCities()
    {
        return self::orderBy('name', 'asc');
    }
}
