<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadStatusHistory extends Model
{
    use SoftDeletes;

    protected $table = 'tb_lead_status_history';
    protected $primaryKey = 'lead_status_history_id';
    protected $fillable = [
        'lead_id',
        'user_id',
        'status'
    ];

    /**
     * @return mixed
     */
    static public function AllFeatures()
    {
        return self::with('UserInfo')->orderBy('feature_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    static public function FeatureIDPriceList()
    {
        return self::pluck('feature_sale_price', 'feature_id');
    }
}
