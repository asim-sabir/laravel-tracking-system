<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageActions extends Model
{
    use SoftDeletes;

    protected $table = 'tb_page_action';
    protected $primaryKey = 'page_action_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'action'
    ];

}
