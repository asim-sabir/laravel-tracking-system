<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $table = 'tb_packages';
    protected $primaryKey = 'package_id';
    protected $fillable = [
        'product_id',
        'user_id',
        'package_name',
        'package_cost',
        'package_sale_price',
        'package_min_cost',
        'package_discount',
        'package_duration',
        'package_description'
    ];

    /**
     * @param $value
     * @return float
     */
    public function getPackageSalePriceAttribute($value)
    {
        return round($value);
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicPackageInfo($select = '*')
    {
        return self::select($select)->orderBy('package_id', 'desc');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllPackages($select = '*')
    {
        return self::BasicPackageInfo($select)->with('UserInfo');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllPackagesWithDetail($select = '*')
    {
        return self::AllPackages($select)->with('PackageFeatures.FeatureInfo');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PackageFeatures()
    {
        return $this->hasMany(PackageFeatures::class, 'package_id', 'package_id');
    }
}
