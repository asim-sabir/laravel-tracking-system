<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $table = 'tb_department';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dept_name'
    ];

    /**
     * @param array $select
     * @return mixed
     */
    static public function AllDepartments($select = ['id', 'dept_name'])
    {
        return self::select($select)->orderBy('id', 'desc');
    }

}
