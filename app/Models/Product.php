<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'tb_products';
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'company_id',
        'user_id',
        'product_name',
        'product_model',
        'description'
    ];

    /**
     * @return mixed
     */
    static public function AllProducts()
    {
        return self::with('UserInfo', 'CompanyInfo')->orderBy('product_id', 'desc');
    }

    /**
     * @return mixed
     */
    static public function AllProductsWithDetail()
    {
        return self::AllProducts()->with('Stock.UserInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CompanyInfo()
    {
        return $this->belongsTo(Company::class, 'company_id', 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Stock()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
