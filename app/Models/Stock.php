<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'tb_stock';
    protected $primaryKey = 'stock_id';
    protected $fillable = [
        'product_id',
        'user_id',
        'unit_serial_no',
        'device_imei',
        'gsm_no',
        'quantity',
        'cost_price',
        'sale_price',
        'mini_sale_price',
    ];

    /**
     * @return mixed
     */
    static public function AllProducts()
    {
        return self::with('UserInfo')->orderBy('company_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
