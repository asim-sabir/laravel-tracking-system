<?php

namespace App\Models;

use App\HelperModules\DateTimeModule;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes, HasRoles;

    protected $guard_name = 'web';
    protected $primaryKey = 'user_id';
    protected $table = 'tb_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social_title',
        'first_name',
        'last_name',
        'email',
        'password',
        'date_of_birthday',
        'department',
        'job_post',
        'mobile',
        'phone',
        'city',
        'state',
        'zip',
        'country',
        'address_line_1',
        'address_line_2',
        'description',
        'linkedin',
        'facebook',
        'twitter',
        'instagram',
        'level',
        'verified',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param $value
     */
    public function setDateOfBirthdayAttribute($value)
    {
        $this->attributes['date_of_birthday'] = DateTimeModule::TimeFormat($value);
    }

    /**
     * @param $value
     * @return null|string
     */
    public function getDateOfBirthdayAttribute($value)
    {
        if($value)
            return DateTimeModule::TimeFormat($value, 'm/d/Y');

        return null;
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    static public function LoginUser()
    {
        return Auth::user();
    }
    /**
     * @return mixed
     */
    static public function AllUsers()
    {
        return self::whereHas('roles', function ($query) {
            $query->where('id', '<>', 1);
        })->with('roles')
            ->orderBy('user_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    static public function UserBasicInfo()
    {
        return self::with('UserDepartment');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserDepartment()
    {
        return $this->belongsTo(Department::class, 'department', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function CustomerAgents()
    {
        return $this->hasMany(CustomerAgents::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function UserLeads()
    {
        return $this->hasMany(LeadAgents::class, 'assigned_to', 'user_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function UserClients()
    {
        return $this->belongsToMany(Client::class, 'tb_customer_agents', 'user_id', 'customer_id')->whereNull('tb_customer_agents.deleted_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function AssignedTicket()
    {
        return $this->hasMany(AssignedTickets::class, 'assigned_to', 'user_id');
    }
}
