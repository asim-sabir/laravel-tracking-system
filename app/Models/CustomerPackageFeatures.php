<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPackageFeatures extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_package_features';
    protected $primaryKey = 'customer_package_feature_id';
    protected $fillable = [
        'customer_package_id',
        'proposal_id',
        'feature_id',
        'feature_name',
        'feature_sale_price',
        'is_free'
    ];

    /**
     * @return mixed
     */
    static public function AllPackages()
    {
        return self::with('UserInfo')->orderBy('package_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function FeatureInfo()
    {
        return $this->belongsTo(Features::class, 'feature_id', 'feature_id');
    }
}
