<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Features extends Model
{
    use SoftDeletes;

    protected $table = 'tb_features';
    protected $primaryKey = 'feature_id';
    protected $fillable = [
        'user_id',
        'feature_name',
        'feature_sale_price',
        'feature_cost_price',
        'feature_description',
        'feature_type'
    ];

    /**
     * @param $value
     * @return float
     */
    public function getFeatureSalePriceAttribute($value)
    {
        return round($value);
    }
    /**
     * @return mixed
     */
    static public function AllFeatures()
    {
        return self::with('UserInfo')->orderBy('feature_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    static public function FeatureIDPriceList()
    {
        return self::pluck('feature_sale_price', 'feature_id');
    }
}
