<?php

namespace App\Models;

use Spatie\Permission\Models\Permission;

class Permissions extends Permission
{
    /**
     * @return mixed
     */
    static public function AllRoles()
    {
        return self::where('name', '<>', 'admin');
    }

}
