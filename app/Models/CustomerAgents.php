<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAgents extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_agents';
    protected $primaryKey = 'customer_agent_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'user_id'
    ];

}
