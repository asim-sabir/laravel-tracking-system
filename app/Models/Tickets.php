<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tickets extends Model
{
    use SoftDeletes;

    protected $table = 'tb_tickets';
    protected $primaryKey = 'ticket_id';
    public $incrementing = false;
    protected $fillable = [
        'ticket_id',
        'created_by',
        'customer_id',
        'dept_id',
        'ticket_title',
        'description',
        'priority_level',
        'status'
    ];

    /**
     * @param $val
     */
    public function setTicketIdAttribute($val)
    {
        $this->attributes['ticket_id'] = HelperModule::CustomID('TC');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function TicketBasicInfo($select = '*')
    {
        return self::select($select)->orderBy('created_by', 'desc');
    }

    /**
     * @param string $select
     * @param string $user_select
     * @return mixed
     */
    static public function TicketsWithUserDetail($select = '*', $user_select = '*')
    {
        return self::TicketBasicInfo($select)
            ->with(['UserInfo' => function($query) use($user_select){
            $query->select($user_select);
        }]);
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function TicketsWithAllDetail($select = '*')
    {
        return self::TicketsWithUserDetail($select)
            ->with('FollowUpHistory', 'StatusHistory', 'AssignedTo.AssignedUserInfo', 'CustomerInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CustomerInfo()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function StatusHistory()
    {
        return $this->hasMany(TicketStatusHistory::class, 'ticket_id', 'ticket_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function FollowUpHistory()
    {
        return $this->hasMany(TicketFollowUpHistory::class, 'ticket_id', 'ticket_id')
            ->orderBy('created_at', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function AssignedTo()
    {
        return $this->hasMany(AssignedTickets::class, 'ticket_id', 'ticket_id');
    }
}
