<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerEmergencyContact extends Model
{
    use SoftDeletes;

    protected $table = 'tb_emergency_contact_person';
    protected $primaryKey = 'contact_id';
    protected $fillable = [
        'customer_id',
        'contact_person_name',
        'contact_relation',
        'mobile_no',
        'phone_no'
    ];

}
