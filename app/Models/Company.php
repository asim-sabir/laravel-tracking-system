<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    use SoftDeletes;
    protected $table = 'company';
    protected $primaryKey = 'company_id';
    protected $fillable = [
        'user_id',
        'name',
        'description'
    ];

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicCompanyInfo($select = '*')
    {
        return self::select($select)->orderBy('company_id', 'desc');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllCompanies($select = '*')
    {
        return self::BasicCompanyInfo($select)->with('UserInfo', 'TotalProducts');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function TotalProducts()
    {
        return $this->hasOne(Product::class, 'company_id', 'company_id')->select(DB::raw('company_id, count(product_id) as count'))->groupBy('company_id');
    }
}
