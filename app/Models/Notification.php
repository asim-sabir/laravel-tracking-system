<?php

namespace App\Models;

use App\HelperModules\DateTimeModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $table = 'tb_notifications';
    protected $fillable = [
        'created_by',
        'dept_id',
        'job_post',
        'message',
        'type',
        'is_seen'
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return DateTimeModule::DiffForHumans($value);
    }
    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicLeadInfo($select = '*')
    {
        return self::select($select)->orderBy('created_at', 'desc');
    }
}
