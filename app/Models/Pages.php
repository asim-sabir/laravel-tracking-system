<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
    use SoftDeletes;

    protected $table = 'tb_pages';
    protected $primaryKey = 'page_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_name',
        'page_route_name',
        'page_slug',
        'level',
        'department'
    ];

    /**
     * @return mixed
     */
    static public function AllPages()
    {
        return self::with('PageAction')->orderBy('page_id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PageAction()
    {
        return $this->hasMany(PageActions::class, 'page_id', 'page_id');
    }
}
