<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Countries\Countries;

class CountriesModel extends Countries
{
    use SoftDeletes;

    /**
     * @return mixed
     */
    static public function AllCountries()
    {
        return self::orderBy('name', 'asc');
    }
}
