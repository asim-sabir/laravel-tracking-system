<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerFollowUpHistory extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_followup_history';
    protected $fillable = [
        'customer_id',
        'user_id',
        'commented_by',
        'comment'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
