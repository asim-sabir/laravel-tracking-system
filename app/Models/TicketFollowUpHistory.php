<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFollowUpHistory extends Model
{
    use SoftDeletes;

    protected $table = 'tb_ticket_followup_history';
    protected $primaryKey = 'id';
    protected $fillable = [
        'ticket_id',
        'user_id',
        'comment',
        'commented_by',
        'is_edit'
    ];
}
