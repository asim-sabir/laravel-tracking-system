<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerVehicleLeadHistory extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_vehicle_lead_history';
    protected $fillable = [
        'vehicle_id',
        'lead_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function VehicleInfo()
    {
        return $this->hasOne(CustomerVehicle::class, 'vehicle_id', 'vehicle_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function LeadInfo()
    {
        return $this->belongsTo(Leads::class, 'lead_id', 'lead_id');
    }
}
