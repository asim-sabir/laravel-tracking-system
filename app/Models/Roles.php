<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Roles extends Role
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    /**
     * @return mixed
     */
    static public function AllRoles()
    {
        return self::where('id', '<>', 1)->orderBy('id', 'desc');
    }

    /**
     * @param $value
     * @return mixed
     */
//    public function getNameAttribute($value)
//    {
//        return str_replace('-',' ', $value);
//    }

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.permission'),
            config('permission.table_names.role_has_permissions'),
            'role_id',
            'permission_id'
        );
    }
}
