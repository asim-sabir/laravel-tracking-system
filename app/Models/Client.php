<?php

namespace App\Models;

use App\HelperModules\DateTimeModule;
use App\HelperModules\HelperModule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer';
    protected $primaryKey = 'customer_id';
    public $incrementing = false;
    protected $fillable = [
        'customer_id',
        'user_id',
        'social_title',
        'customer_name',
        'id_card_no',
        'ntn_no',
        'date_of_birthday',
        'designation',
        'company_name',
        'email',
        'mobile_1',
        'mobile_2',
        'phone_1',
        'phone_2',
        'city',
        'state',
        'zip',
        'country',
        'address_line_1',
        'address_line_2',
        'customer_type',
        'verified',
        'status'
    ];

    /**
     * @param $val
     */
    public function setCustomerIdAttribute($val)
    {
        $this->attributes['customer_id'] = HelperModule::CustomID();
    }
    /**
     * @param $value
     */
    public function setDateOfBirthdayAttribute($value)
    {
        $this->attributes['date_of_birthday'] = DateTimeModule::TimeFormat($value);
    }

    /**
     * @param $value
     * @return null|string
     */
    public function getDateOfBirthdayAttribute($value)
    {
        if($value)
            return DateTimeModule::TimeFormat($value, 'm/d/Y');

        return null;
    }
    /**
     * @param string $select
     * @return mixed
     */
    static public function ClientBasicInfo($select = '*')
    {
        return self::select($select)->orderBy('customer_id', 'desc');
    }

    /**
     * @param $user_id
     * @param string $select
     * @return mixed
     */
    static public function GetClientByUserId($user_id, $select = '*')
    {
        return self::ClientBasicInfo($select)
            ->whereHas('Agents', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        })
            ->where('user_id', $user_id);
    }

    static public function ClientInfoWithAllDetail($select = '*')
    {
        return self::ClientBasicInfo($select)->with('FollowUpHistory.UserInfo', 'EmergencyContact');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function Agents()
    {
        return $this->hasMany(CustomerAgents::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Stock()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function FollowUpHistory()
    {
        return $this->hasMany(CustomerFollowUpHistory::class, 'customer_id', 'customer_id')
            ->orderBy('created_at', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Proposals()
    {
        return $this->hasMany(Proposal::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function EmergencyContact()
    {
        return $this->hasMany(CustomerEmergencyContact::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function CustomerVehicles()
    {
        return $this->hasMany(CustomerVehicle::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ClientAgents()
    {
        return $this->belongsToMany(User::class, 'tb_customer_agents', 'customer_id', 'user_id')->whereNull('tb_customer_agents.deleted_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function ActiveOperationalTicket()
    {
        return $this->hasMany(OperationalTickets::class, 'customer_id', 'customer_id')
            ->where('status', 1);
    }
}
