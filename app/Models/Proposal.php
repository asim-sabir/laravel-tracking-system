<?php

namespace App\Models;

use App\HelperModules\DateTimeModule;
use App\HelperModules\FileModules;
use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proposal extends Model
{
    use SoftDeletes;

    protected $table = 'tb_proposal';
    protected $primaryKey = 'proposal_id';
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $fillable = [
        'proposal_id',
        'customer_id',
        'user_id',
        'name',
        'proposal_sale_price',
        'proposal_discount',
        'description',
        'pdf_file_path',
        'status'
    ];

    /**
     * @param $value
     */
    public function setProposalIdAttribute($value)
    {
        $this->attributes['proposal_id'] = HelperModule::CustomID('PR');
    }

    /**
     * @param $value
     * @return string
     */
    public function getPdfFilePathAttribute($value)
    {
        if ($value)
            return FileModules::GetFilePath('uploads/proposals/' . $value);

        return null;
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        if ($value)
            return DateTimeModule::TimeFormat($value, 'd/m/Y');

        return null;
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicProposalInfo($select = "*")
    {
        return self::select($select)->orderBy('proposal_id', 'desc');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllProposal($select = "*")
    {
        return self::BasicProposalInfo($select)->with('UserInfo');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllProposalWithDetail($select = "*")
    {
        return self::AllProposal($select)->with('ClientInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ClientInfo()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ProposalPackage()
    {
        return $this->hasOne(CustomerPackage::class, 'proposal_id', 'proposal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ProposalLead()
    {
        return $this->hasOne(Leads::class, 'proposal_id', 'proposal_id');
    }
}
