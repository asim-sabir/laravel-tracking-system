<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperationalTickets extends Model
{
    use SoftDeletes;

    protected $table = 'tb_operational_ticket';
    protected $primaryKey = 'operational_ticket_id';
    public $incrementing = false;
    protected $fillable = [
        'operational_ticket_id',
        'created_by',
        'dept_id',
        'vehicle_id',
        'customer_id',
        'ticket_title',
        'description',
        'priority_level',
        'status',
        'type'
    ];

    /**
     * @param $val
     */
    public function setOperationalTicketIdAttribute($val)
    {
        $this->attributes['operational_ticket_id'] = HelperModule::CustomID('OTC');
    }

    /**
     * @param $val
     */
    public function setTypeAttribute($val)
    {
        switch ($val) {
            case 0:
                $this->attributes['dept_id'] = 2;
                break;
            case 1:
                $this->attributes['dept_id'] = 2;
                break;
            case 2:
                $this->attributes['dept_id'] = 2;
                break;
            case 3:
                $this->attributes['dept_id'] = 0;
                break;
            case 4:
                $this->attributes['dept_id'] = 0;
                break;
            case 5:
                $this->attributes['dept_id'] = 0;
                break;
            default:
                $this->attributes['dept_id'] = 0;
        }
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function TicketBasicInfo($select = '*')
    {
        return self::select($select)->orderBy('created_by', 'desc');
    }

    /**
     * @param string $select
     * @param string $user_select
     * @return mixed
     */
    static public function TicketsWithUserDetail($select = '*', $user_select = '*')
    {
        return self::TicketBasicInfo($select)
            ->with(['UserInfo' => function ($query) use ($user_select) {
                $query->select($user_select);
            }]);
    }

    /**
     * @param $request
     * @param $type
     * @param $status
     * @return mixed
     */
    static public function CheckTicketExist($request, $type, $status)
    {
        return self::TicketBasicInfo()->where(function ($where) use ($request, $type, $status) {
            $where->where('operational_ticket_id', $request->opt_id)
                ->whereHas('AssignedTo', function ($query) use ($request) {
                    $query->where('assigned_to', $request->user_id);
                })
                ->where('type', $type)
                ->where('status', $status);
        });
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function TicketsWithAllDetail($select = '*')
    {
        return self::TicketsWithUserDetail($select)
            ->with('FollowUpHistory', 'StatusHistory', 'AssignedTo.AssignedUserInfo', 'CustomerInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CustomerInfo()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function StatusHistory()
    {
        return $this->hasMany(TicketStatusHistory::class, 'ticket_id', 'operational_ticket_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function FollowUpHistory()
    {
        return $this->hasMany(TicketFollowUpHistory::class, 'ticket_id', 'operational_ticket_id')
            ->orderBy('created_at', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function AssignedTo()
    {
        return $this->hasMany(AssignedTickets::class, 'ticket_id', 'operational_ticket_id');
    }
}
