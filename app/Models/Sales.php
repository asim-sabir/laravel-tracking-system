<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales extends Model
{
    use SoftDeletes;

    protected $table = 'tb_sales';
    protected $primaryKey = 'sale_id';
    public $incrementing = false;
    protected $fillable = [
        'lead_id',
        'customer_id',
        'user_id'
    ];

    /**
     * @param $value
     */
    public function setSaleIdAttribute($value)
    {
        $this->attributes['sale_id'] = HelperModule::CustomID('SL');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicSaleInfo($select = '*')
    {
        return self::select($select)->orderBy('lead_id', 'desc');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllSale($select = '*')
    {
        return self::BasicSaleInfo($select)->with('ClientInfo');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function AllSaleWithDetail($select = '*')
    {
        return self::AllSale($select)->with('Lead.LeadProposal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ClientInfo()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Lead()
    {
        return $this->belongsTo(Leads::class, 'lead_id', 'lead_id');
    }
}
