<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotifications extends Model
{
    use SoftDeletes;

    protected $table = 'tb_user_notification';
    protected $primaryKey = 'user_notification_id';
    protected $fillable = [
        'id',
        'user_id',
        'is_seen'
    ];
}
