<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPackage extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_package';
    protected $primaryKey = 'customer_package_id';
    protected $fillable = [
        'proposal_id',
        'package_id',
        'product_name',
        'package_name',
        'package_cost',
        'package_sale_price',
        'package_min_cost',
        'package_discount',
        'package_duration',
        'package_description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PackageFeatures()
    {
        return $this->hasMany(CustomerPackageFeatures::class, 'customer_package_id', 'customer_package_id');
    }
}
