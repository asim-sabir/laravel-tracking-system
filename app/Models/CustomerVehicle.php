<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerVehicle extends Model
{
    use SoftDeletes;

    protected $table = 'tb_customer_vehicle';
    protected $primaryKey = 'vehicle_id';
    public $incrementing = false;
    protected $fillable = [
        'vehicle_id',
        'customer_id',
        'plate_no',
        'year_of_manufacturing',
        'vehicle_make',
        'vehicle_model',
        'vehicle_color',
        'vehicle_engine_cc',
        'vehicle_engine_no',
        'vehicle_chassis_no',
        'fuel_type',
        'transmission_type'
    ];

    /**
     * @param $val
     */
    public function setVehicleIdAttribute($val)
    {
        if ($val)
            $this->attributes['vehicle_id'] = HelperModule::CustomID('VE') . $val;
        else
            $this->attributes['vehicle_id'] = HelperModule::CustomID('VE');
    }

    /**
     * @param string $select
     * @return mixed
     */
    static public function BasicVehicleInfo($select = '*')
    {
        return self::select($select)->orderBy('vehicle_id', 'desc');
    }

    /**
     * @param $lead_id
     * @param string $select
     * @return mixed
     */
    static public function GetVehicleByLeadId($lead_id, $select = '*')
    {
        return self::BasicVehicleInfo($select)
            ->whereHas('LeadHistory', function ($query) use ($lead_id) {
                $query->where('lead_id', $lead_id)
                    ->where('status', 1);
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function LeadHistory()
    {
        return $this->hasMany(CustomerVehicleLeadHistory::class, 'vehicle_id', 'vehicle_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function ActiveLead()
    {
        return $this->hasOne(CustomerVehicleLeadHistory::class, 'vehicle_id', 'vehicle_id')->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Customer()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'customer_id');
    }
}
