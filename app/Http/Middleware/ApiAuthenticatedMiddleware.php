<?php

namespace App\Http\Middleware;

use App\HelperModules\HelperModule;
use Closure;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\Authenticate;

class ApiAuthenticatedMiddleware extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken())
            return response(HelperModule::jsonResponse(false,Lang::get('auth.invalid', ['attribute' => 'token'])));
        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return response(HelperModule::jsonResponse(false,Lang::get('auth.expire', ['attribute' => 'Token'])));
        } catch (JWTException $e) {
            return response(HelperModule::jsonResponse(false,Lang::get('auth.invalid', ['attribute' => 'token'])));
        }

        if (!$user)
            return response(HelperModule::jsonResponse(false,Lang::get('auth.failed')));

        //add user id into request
        $request->user_id = $user->user_id;

        return $next($request);
    }
}
