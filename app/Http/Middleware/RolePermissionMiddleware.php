<?php

namespace App\Http\Middleware;

use App\HelperModules\HelperModule;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class RolePermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (Auth::user()->hasRole('Admin'))
            return $next($request);

        if (!Auth::user()->can($permission))
            return response(HelperModule::jsonResponse(false, Lang::get('messages.error.permission')));

        return $next($request);
    }
}
