<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        try{
            if($token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $data = [
                    'info' => $this->UserInfo(),
                    'token' => $token
                ];
                return HelperModule::jsonResponse(true, false, $data);
            }
        }catch (JWTException $e){
            return HelperModule::jsonResponse(false,Lang::get('auth.token'));
        }

        return HelperModule::jsonResponse(false, Lang::get('auth.failed'));
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function CheckLogin(Request $request)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
            if(!$user)
                return HelperModule::jsonResponse(false, Lang::get('auth.failed'));
        }catch (TokenExpiredException $e){
            return HelperModule::jsonResponse(false, Lang::get('auth.expire', ['attribute' => 'Token']));

        }catch (TokenInvalidException $e){
            return HelperModule::jsonResponse(false, Lang::get('auth.invalid', ['attribute' => 'token']));

        }catch (JWTException $e){
            return HelperModule::jsonResponse(false, Lang::get('auth.failed'));

        }

        return HelperModule::jsonResponse(true,false, $this->UserInfo());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function logout()
    {
        Auth::logout();
        return HelperModule::jsonResponse(true, false, false);

    }
    /**
     * @return mixed
     */
    private function UserInfo()
    {
        $dept = '';
        $user = User::UserBasicInfo()->where('user_id', Auth::user()->user_id)->first();
        if($user->UserDepartment)
            $dept = $user->UserDepartment->dept_name;

        return [
            'social_title' => $user->social_title,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'post' => $user->job_post,
            'dept_id' => $user->department,
            'dept' => $dept,
            'level' => $user->level,
            'roles' => $user->roles,
            'permissions' => $user->getAllPermissions()
        ];
    }
}
