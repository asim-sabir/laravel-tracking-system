<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeatureRequest;
use App\Models\Features;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class FeatureController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function FeaturesList()
    {
        $features = Features::AllFeatures()->get();
        if (!count($features))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $features);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new FeatureRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $feature = Features::create($request->all());
        if (!$feature)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Feature']));
    }

    /**
     * @param Features $feature
     * @return \Illuminate\Support\Collection
     */
    public function FeatureInfo(Features $feature)
    {
        if (!$feature)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $feature);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new FeatureRequest();
        $rules = $request_obj->rules();
        $rules['feature_name'] = 'required|unique:tb_features,feature_name,' . $request->id . ',feature_id';
        $rules['id'] = 'required|exists:tb_features,feature_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());

        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $feature = Features::find($request->id);
        if (!$feature)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $feature->update($request->all());

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Feature']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_features,feature_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $feature = Features::find($request->id)->delete();
        if (!$feature)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Feature']));
    }
}
