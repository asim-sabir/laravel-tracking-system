<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function CompaniesList()
    {
        $companies = Company::AllCompanies()->get();
        if(!count($companies))
            return HelperModule::jsonResponse(false,Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $companies);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:company,name'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $company = Company::create($request->all());
        if(!$company)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Company']));
    }

    /**
     * @param Company $company
     * @return \Illuminate\Support\Collection
     */
    public function CompanyInfo(Company $company)
    {
        if(!$company)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $company);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:company,company_id',
            'name' => 'required|unique:company,name,'. $request->id .',company_id'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $company = Company::find($request->id)->update($request->all());
        if(!$company)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Company']));
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:company,company_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $company = Company::find($request->id)->delete();
        if(!$company)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Company']));
    }
}
