<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Department;
use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function UserList()
    {
        $users = User::AllUsers()->get();
        if (!count($users))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $users);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $roles = Roles::AllRoles()->get();
        $department = Department::AllDepartments()->get();
        if (!count($roles))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'role']));

        $data = [
            'roles' => $roles,
            'department' => $department
        ];

        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new UserRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['password'] = bcrypt($request->password);
        $user = User::create($request->all());
        //assign role to user
        $role = str_replace(' ', '-', $request->role);
        $user->assignRole($role);
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'User']));
    }

    /**
     * @param $user_id
     * @return \Illuminate\Support\Collection
     */
    public function UserInfo($user_id)
    {
        $user_info = User::AllUsers()->where('user_id', $user_id)->first();
        if (!$user_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $user_info);
    }

    /**
     * @param $user_id
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate($user_id)
    {
        $roles = Roles::AllRoles()->get();
        $user_info = User::AllUsers()->where('user_id', $user_id)->first();
        $department = Department::AllDepartments()->get();

        if (!count($roles))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'roles']));
        if (!$user_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'user']));

        $data = [
            'roles' => $roles,
            'user_info' => $user_info,
            'department' => $department
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new UserRequest();
        $rules = $request_obj->rules();
        $rules['email'] = 'required|email|unique:tb_users,email,' . $request->id . ',user_id';
        if($request->filled('update_password'))
            $request['password'] = bcrypt($request->update_password);
        else
            unset($rules['password']);

        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = User::find($request->id)->update($request->all());
        //sync user role
        $role = str_replace(' ', '-', $request->role);
        User::find($request->id)->syncRoles([$role]);
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'User']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_users,user_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = User::find($request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'User']));
    }
}
