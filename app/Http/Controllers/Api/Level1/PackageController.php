<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Models\Features;
use App\Models\Package;
use App\Models\PackageFeatures;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function PackageList()
    {
        $packages = Package::AllPackages()->get();
        if (!count($packages))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $packages);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $features = Features::get();
        $products = Product::get();
        if (!count($features))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'features']));
        if (!count($products))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'products']));

        $data = [
            'products' => $products,
            'features' => $features
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new PackageRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $request['package_cost'] = 0;
        $package = Package::create($request->all());
        if (!$package)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        /* get features */
        $features = Features::FeatureIDPriceList();
        if (count($features) > 0) {
            $package_cost = 0;
            /*save feature information*/
            for ($i = 0; $i < count($request->feature_id); $i++) {
                $is_free = 0;
                if (isset($request->is_free[$i]))
                    $is_free = 1;
                $package_cost = $package_cost + $features[$request->feature_id[$i]];
                $data [] = [
                    'package_id' => $package->package_id,
                    'feature_id' => $request->feature_id[$i],
                    'feature_sale_price' => $features[$request->feature_id[$i]],
                    'is_free' => $is_free
                ];
            }
            Package::find($package->package_id)->update(['package_cost' => $package_cost]);
            PackageFeatures::insert($data);
        }
        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Package']));
    }

    /**
     * @param $package_id
     * @return \Illuminate\Support\Collection
     */
    public function PackageInfo($package_id)
    {
        $package_info = Package::AllPackagesWithDetail()->where('package_id', $package_id)->first();
        if (!$package_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $package_info);
    }

    /**
     * @param $package_id
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate($package_id)
    {
        $features = Features::get();
        $products = Product::get();
        $package = Package::with('PackageFeatures')->where('package_id', $package_id)->first();
        if (!count($features))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'features']));
        if (!count($products))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'products']));
        if (!count($package))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'package']));

        $data = [
            'products' => $products,
            'features' => $features,
            'package' => $package
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new PackageRequest();
        $rules = $request_obj->rules();
        $rules['package_name'] = 'required|unique:tb_packages,package_name,' . $request->id . ',package_id';
        $rules['id'] = 'required|exists:tb_packages,package_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());

        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $package = Package::find($request->id);
        if (!$package)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $package->update($request->all());
        /* get features */
        $features = Features::FeatureIDPriceList();
        if (count($features) > 0) {
            PackageFeatures::where('package_id', $request->id)->delete();
            $package_cost = 0;
            /*save feature information*/
            for ($i = 0; $i < count($request->feature_id); $i++) {
                $is_free = 0;
                if (isset($request->is_free[$i]))
                    $is_free = 1;
                $package_cost = $package_cost + $features[$request->feature_id[$i]];
                $data [] = [
                    'package_id' => $request->id,
                    'feature_id' => $request->feature_id[$i],
                    'feature_sale_price' => $features[$request->feature_id[$i]],
                    'is_free' => $is_free
                ];
            }
            Package::find($request->id)->update(['package_cost' => $package_cost]);
            PackageFeatures::insert($data);
        }

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Package']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_packages,package_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $packages = Package::find($request->id)->delete();
        if (!$packages)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Package']));
    }
}
