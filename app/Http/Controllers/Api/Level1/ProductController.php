<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function ProductsList()
    {
        $products = Product::AllProducts()->get();
        if(!count($products))
            return HelperModule::jsonResponse(false,Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $products);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new ProductRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $product = Product::create($request->all());
        if(!$product)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Product']));
    }

    /**
     * @param Product $product
     * @return \Illuminate\Support\Collection
     */
    public function ProductInfo(Product $product)
    {
        if(!$product)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));
        $companies = Company::get();
        $data = [
            'product_info' => $product,
            'companies' => $companies
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param $product_id
     * @return \Illuminate\Support\Collection
     * @internal param Request $request
     */
    public function View($product_id)
    {
        $product = Product::AllProducts()->where('product_id', $product_id)->first();
        if(!$product)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $product);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new ProductRequest();
        $rules = $request_obj->rules();
        $rules['id'] = 'required|exists:tb_products,product_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());

        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $product = Product::find($request->id)->update($request->all());
        if(!$product)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Company']));
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_products,product_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $product = Product::find($request->id)->delete();
        if(!$product)
            return HelperModule::jsonResponse(false,Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Product']));
    }
}
