<?php

namespace App\Http\Controllers\Api\Level1;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Department;
use App\Models\Pages;
use App\Models\Permissions;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RolePermissionController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function RoleList()
    {
        $roles = Roles::AllRoles()->get();
        if (!count($roles))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'role']));

        return HelperModule::jsonResponse(true, false, $roles);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $pages = Pages::AllPages()->get();
        $department = Department::AllDepartments()->get();
        if (!count($pages))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'permission']));
        $data = [
            'pages' => $pages,
            'department' => $department
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $request_obj = new RoleRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());
        $exist_role = Roles::where('name', str_replace(' ', '-', $request->name))->first();
        if ($exist_role)
            return HelperModule::jsonResponse(false, Lang::get('validation.unique', ['attribute' => 'role']));
        $role = Role::create([
            'name' => str_replace(' ', '-', $request->name),
            'level' => $request->level,
            'department' => $request->department,
            'description' => $request->description
        ]);
        if (count($request->permission) > 0) {
            for ($i = 0; $i < count($request->permission); $i++) {
                $value = str_replace(' ', '_', $request->permission[$i]);
                if (!Permissions::where('name', $value)->first())
                    Permissions::create([
                        'name' => $value,
                        'action' => $request->action[$i],
                    ]);
                $role->givePermissionTo($request->permission[$i]);
            }
        }
        if (!$role)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Role']));
    }

    /**
     * @param $role_id
     * @return \Illuminate\Support\Collection
     */
    public function RoleInfo($role_id)
    {
        $final_array = [];
        $permissions_array = [];
        $role = Roles::find($role_id);
        $permissions = $role->permissions()->pluck('name')->toArray();
        if (!count($role))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'role']));
        foreach ($permissions as $key => $val) {
            $names_array = $this->StringSetting($val);
            if ($this->FindInArray($final_array, $names_array['page_name'])) {
                $permissions_array[] = $names_array['permission_name'];
                $final_array[count($final_array) - 1] = [
                    'name' => $names_array['page_name'],
                    'permissions' => $permissions_array
                ];
            } else {
                $permissions_array = [];
                $permissions_array[] = $names_array['permission_name'];
                $final_array[] = [
                    'name' => $names_array['page_name'],
                    'permissions' => $permissions_array
                ];
            }
        }
        $data = [
            'role' => $role,
            'permissions' => $final_array
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param $role_id
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate($role_id)
    {
        $role = Role::find($role_id);
        $permissions = $role->permissions()->pluck('name')->toArray();
        $permissions = array_flip($permissions);
        $pages = Pages::AllPages()->get();
        $department = Department::AllDepartments()->get();
        if (!count($role))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'role']));

        $data = [
            'role' => $role,
            'permissions' => $permissions,
            'pages' => $pages,
            'department' => $department
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new RoleRequest();
        $rules = $request_obj->rules();
        $rules['name'] = 'required|unique:roles,name,' . $request->id . ',id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $role = Role::find($request->id);
        $role->update([
            'name' => str_replace(' ', '-', $request->name),
            'level' => $request->level,
            'department' => $request->department,
            'description' => $request->description
        ]);
        //revoke all permissions
        $p_all = Permissions::get();
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }
        if (count($request->permission) > 0) {
            for ($i = 0; $i < count($request->permission); $i++) {
                $value = str_replace(' ', '_', $request->permission[$i]);
                if (!Permissions::where('name', $value)->first())
                    Permissions::create([
                        'name' => $value,
                        'action' => $request->action[$i],
                    ]);
                $role->givePermissionTo($request->permission[$i]);
            }
        }
        if (!$role)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Role']));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function Destroy($id)
    {
        $data['id'] = $id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:roles,id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $role = Roles::find($id);
        if (!$role)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        $role->delete();
        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Role']));
    }

    /**
     * @param $data
     * @return array
     */
    private function StringSetting($data)
    {
        $data = str_replace('_', ' ', $data);
        $data = explode(' ', $data);
        $permission_name = $data[count($data) - 1];
        array_pop($data);
        $page_name = implode(' ', $data);
        return [
            'permission_name' => $permission_name,
            'page_name' => $page_name,
        ];
    }

    private function FindInArray($array_data, $name)
    {
        foreach ($array_data as $data) {
            if ($data['name'] == $name)
                return true;
        }
        return false;
    }
}
