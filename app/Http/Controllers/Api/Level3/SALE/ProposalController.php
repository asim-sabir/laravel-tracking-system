<?php

namespace App\Http\Controllers\Api\Level3\SALE;

use App\HelperModules\FileModules;
use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Http\Requests\ProposalRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\CountriesModel;
use App\Models\Package;
use App\Models\Proposal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Barryvdh\Snappy\Facades\SnappyPdf as Pdf;

class ProposalController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function ProposalList(Request $request)
    {
        $proposals = Proposal::AllProposalWithDetail()->where('user_id', $request->user_id)->get();
        if (!count($proposals))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $proposals);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd(Request $request)
    {
        $client_select = ['customer_id', 'user_id', 'customer_name', 'company_name'];
        $package_select = ['package_id', 'package_name'];
        $clients = Client::GetClientByUserId($request->user_id, $client_select)->get();
        $packages = Package::BasicPackageInfo($package_select)->get();

        if (!count($clients))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'clients']));
        if (!count($packages))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'packages']));

        $data = [
            'clients' => $clients,
            'packages' => $packages
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new ProposalRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());
        //get package information
        $package = Package::AllPackagesWithDetail()->where('package_id', $request->package_id)->first();
        $request['user_id'] = $request->user_id;
        $request['proposal_id'] = '';
        $request['proposal_sale_price'] = $package->package_sale_price;
        $proposal = Proposal::create($request->all());
        // save proposal package
        $proposal_package = $proposal->ProposalPackage()->create([
            'package_id'  => $package->package_id,
            'package_name' => $package->package_name,
            'package_cost' => $package->package_cost,
            'package_sale_price' => $package->package_sale_price,
            'package_min_cost' => $package->package_min_cost,
            'package_discount' => $package->package_discount,
            'package_duration' => $package->package_duration,
            'package_description' => $package->package_description
        ]);
        $data = [];
        foreach ($package->PackageFeatures as $val){
            $data [] = [
                'proposal_id' => $proposal->proposal_id,
                'feature_id' => $val->feature_id,
                'feature_name' => $val->FeatureInfo->feature_name,
                'feature_sale_price' => $val->feature_sale_price,
                'is_free' => $val->is_free,
            ];
        }
        // save package features
        $proposal_package->PackageFeatures()->createMany($data);
        //generate proposal pdf
        $pdf = Pdf::loadView('proposal.proposal', ['package' => $package, 'proposal' => $proposal]);
        $path = $proposal->proposal_id.'-proposal.pdf';
        $pdf->save(base_path('public/uploads/proposals/'.$path));
        //update proposal table
        $proposal->pdf_file_path = $path;
        $proposal->save();
        $path = FileModules::GetFilePath('uploads/proposals/'.$path);

        if (!$proposal)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new proposal has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Proposal']), $path);
    }

    /**
     * @param Request $request
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function ProposalInfo(Request $request, $client_id)
    {
        $client = Client::GetClientByUserId($request->user_id)->where('customer_id', $client_id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $client);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_proposal,proposal_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $proposal = Proposal::where('user_id', $request->user_id)
            ->where('status', 0)->where('proposal_id', $request->id)->delete();
        if (!$proposal)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Proposal']));
    }
}
