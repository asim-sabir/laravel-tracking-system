<?php

namespace App\Http\Controllers\Api\Level3\SALE;

use App\HelperModules\DateTimeModule;
use App\HelperModules\FileModules;
use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use App\Models\Client;
use App\Models\Leads;
use App\Models\Proposal;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Reports(Request $request)
    {
        $request_obj = new ReportRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());
        if($request->category == 1)
            return $this->ClientsReport($request);
        if($request->category == 2)
            return $this->ProposalsReport($request);
        if($request->category == 3)
            return $this->LeadsReport($request);
        if($request->category == 4)
            return $this->SalesReport($request);

        return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
    }

    /**
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function ClientsReport($request)
    {
        $clients = Client::GetClientByUserId($request->user_id)
            ->where(function ($query) use($request){
                $query->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '>=', $request->from_date)
                    ->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '<=', $request->to_date);
            })
            ->get();
        $file = 'Clients-' . DateTimeModule::StrToTime();
        $excel = Excel::create($file, function ($excel) use ($clients) {
            $excel->sheet('Client Report', function ($sheet) use ($clients) {
                $sheet->loadView('reports.clients', compact('clients'));
            });
        });
        $excel->store('xlsx', base_path('public/uploads/reports/'.$file));
        $path = FileModules::GetFilePath('uploads/reports/'.$file.'/'.$file.'.xlsx');
        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Report']), $path);
    }

    /**
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function ProposalsReport($request)
    {
        $proposals = Proposal::AllProposalWithDetail()->where('user_id', $request->user_id)
            ->where(function ($query) use($request){
                $query->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '>=', $request->from_date)
                    ->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '<=', $request->to_date);
            })
            ->get();
        $file = 'Proposals-' . DateTimeModule::StrToTime();
        $excel = Excel::create($file, function ($excel) use ($proposals) {
            $excel->sheet('Proposals Report', function ($sheet) use ($proposals) {
                $sheet->loadView('reports.proposals', compact('proposals'));
            });
        });
        $excel->store('xlsx', base_path('public/uploads/reports/'.$file));
        $path = FileModules::GetFilePath('uploads/reports/'.$file.'/'.$file.'.xlsx');
        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Report']), $path);
    }

    /**
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function LeadsReport($request)
    {
        $leads = Leads::AllLeads()->where('user_id', $request->user_id)
            ->where(function ($query) use($request){
                $query->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '>=', $request->from_date)
                    ->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '<=', $request->to_date);
            })
            ->get();
        $file = 'Leads-' . DateTimeModule::StrToTime();
        $excel = Excel::create($file, function ($excel) use ($leads) {
            $excel->sheet('Leads Report', function ($sheet) use ($leads) {
                $sheet->loadView('reports.leads', compact('leads'));
            });
        });
        $excel->store('xlsx', base_path('public/uploads/reports/'.$file));
        $path = FileModules::GetFilePath('uploads/reports/'.$file.'/'.$file.'.xlsx');
        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Report']), $path);
    }

    /**
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function SalesReport($request)
    {
        $sales = Sales::AllSaleWithDetail()->where('user_id', $request->user_id)
            ->where(function ($query) use($request){
                $query->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '>=', $request->from_date)
                    ->where(DB::raw('FORMAT(created_at, \'MM/dd/yyyy\')'), '<=', $request->to_date);
            })
            ->get();
        $file = 'Sales-' . DateTimeModule::StrToTime();
        $excel = Excel::create($file, function ($excel) use ($sales) {
            $excel->sheet('Sale Report', function ($sheet) use ($sales) {
                $sheet->loadView('reports.sales', compact('sales'));
            });
        });
        $excel->store('xlsx', base_path('public/uploads/reports/'.$file));
        $path = FileModules::GetFilePath('uploads/reports/'.$file.'/'.$file.'.xlsx');
        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Report']), $path);
    }
}
