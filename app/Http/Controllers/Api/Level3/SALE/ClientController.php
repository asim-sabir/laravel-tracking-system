<?php

namespace App\Http\Controllers\Api\Level3\SALE;

use App\Events\ClientNotification;
use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\CountriesModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function ClientList(Request $request)
    {
        $clients = Client::GetClientByUserId($request->user_id)->get();
        if (!count($clients))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $clients);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));

        $data = [
            'countries' => $countries,
            'cities' => $cities
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new ClientRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $request['customer_id'] = '';
        $client = Client::create($request->all());
        $client->Agents()->create([
            'user_id' => $request->user_id
        ]);

        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
        $user = User::LoginUser();
        NotificationModule::ClientNotification($user, 'New client has been added', 'hod');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Client']));
    }

    /**
     * @param Request $request
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function ClientInfo(Request $request, $client_id)
    {
        $client = Client::GetClientByUserId($request->user_id)
            ->with(['FollowUpHistory.UserInfo' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name']);
            }])
            ->where('customer_id', $client_id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $client);
    }

    /**
     * @param Request $request
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate(Request $request, $client_id)
    {
        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        $client = Client::GetClientByUserId($request->user_id)->where('customer_id', $client_id)->first();

        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'client' => $client
        ];

        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new ClientRequest();
        $rules = $request_obj->rules();
        $rules['id'] = 'required|exists:tb_customer,customer_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $client = Client::GetClientByUserId($request->user_id)->where('customer_id', $request->id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $client = $client->update($request->all());

        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Client']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_customer,customer_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = Client::GetClientByUserId($request->user_id)->where('customer_id', $request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Client']));
    }
}
