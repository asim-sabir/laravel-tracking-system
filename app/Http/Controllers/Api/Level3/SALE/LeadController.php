<?php

namespace App\Http\Controllers\Api\Level3\SALE;

use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use App\Models\Leads;
use App\Models\Proposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function LeadList(Request $request)
    {
        $leads = Leads::AllLeads()->where('user_id', $request->user_id)->get();
        if (!count($leads))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'lead']));

        return HelperModule::jsonResponse(true, false, $leads);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd(Request $request)
    {
        $proposals = Proposal::BasicProposalInfo()->where('user_id', $request->user_id)->where('status', 0)->get();
        if (!count($proposals))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $proposals);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new LeadRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        // get proposal info
        $proposal = Proposal::where('proposal_id', $request->proposal_id)->where('user_id', $request->user_id)->first();
        if (!$proposal)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $proposal->status = 1;
        $proposal->save();

        //save lead
        $request['user_id'] = $request->user_id;
        $request['lead_id'] = '';
        $lead = $proposal->ProposalLead()->create($request->all());
        // assign lead to agent
        $lead->Agents()->create([
            'assigned_by' => $request->user_id,
            'assigned_to' => $request->user_id,
        ]);
        //save lead status history
        $lead_status = $lead->LeadStatusHistory()->create($request->all());
        //save lead follow up history
        if ($request->filled('comment'))
            $lead->LeadFollowUpHistory()->create($request->all());

        if (!$proposal || !$lead || !$lead_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Lead']));
    }

    /**
     * @param Request $request
     * @param $lead_id
     * @return \Illuminate\Support\Collection
     */
    public function LeadInfo(Request $request, $lead_id)
    {
        $user_select = ['user_id', 'first_name', 'last_name'];
        $leads = Leads::AllLeadsWithDetail('*', $user_select)
            ->where('lead_id', $lead_id)->where('user_id', $request->user_id)->first();
        if (!$leads)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $leads);
    }

}
