<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetInfo()
    {
        return HelperModule::jsonResponse(true,false, Auth::user());
    }
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new UserRequest();
        $rules = $request_obj->rules();
        unset($rules['email']);
        unset($rules['role']);
        unset($rules['department']);
        unset($rules['job_post']);
        unset($rules['level']);
        if($request->filled('update_password'))
            $request['password'] = bcrypt($request->update_password);
        else
            unset($rules['password']);

        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = User::find($request->user_id)->update($request->all());

        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'User']), $this->UserInfo($request->user_id));
    }
    /**
     * @return mixed
     */
    private function UserInfo($id)
    {
        $user = User::find($id);
        return [
            'social_title' => $user->social_title,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email
        ];
    }
}
