<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SaleController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function SaleList(Request $request)
    {
        $sales = Sales::AllSaleWithDetail()->where('user_id', $request->user_id)->get();
        if (!count($sales))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $sales);
    }

    /**
     * @param Request $request
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function SaleInfo(Request $request, $client_id)
    {
        $client = Client::GetClientByUserId($request->user_id)->where('customer_id', $client_id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $client);
    }

}
