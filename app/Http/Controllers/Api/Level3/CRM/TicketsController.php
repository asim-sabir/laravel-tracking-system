<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\TicketRequest;
use App\Http\Requests\UserRequest;
use App\Models\AssignedTickets;
use App\Models\Client;
use App\Models\CustomerAgents;
use App\Models\Department;
use App\Models\Roles;
use App\Models\Tickets;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TicketsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function TicketList(Request $request)
    {
        $tickets = Tickets::TicketsWithUserDetail('*', ['user_id', 'first_name', 'last_name'])
            ->with(['AssignedTo' => function ($query) {
                $query->select(['ticket_id', 'assigned_to']);
                $query->with(['AssignedUserInfo' => function ($user_query) {
                    $user_query->select(['user_id', 'first_name', 'last_name']);
                }]);
            }])
            ->where('created_by', $request->user_id)
            ->get();
        $assigned_tickets = Tickets::TicketsWithUserDetail('*', ['user_id', 'first_name', 'last_name'])
            ->whereHas('AssignedTo', function ($query) use($request){
                $query->where('assigned_to', $request->user_id);
            })->get();
        if (!count($tickets) && !count($assigned_tickets))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'ticket record']));

        $data = [
            'tickets' => $tickets,
            'assigned_tickets' => $assigned_tickets,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $client = Client::ClientBasicInfo(['customer_id', 'customer_name'])->get();
        $dept = Department::AllDepartments()->get();
        if (!count($client))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client record']));
        $data = [
            'client' => $client,
            'dept' => $dept,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new TicketRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //save ticket
        $request['created_by'] = $request->user_id;
        $request['ticket_id'] = '';
        $request['status'] = 0;
        $ticket = Tickets::create($request->all());
        // save status history
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Ticket']));
    }

    /**
     * @param $ticket_id
     * @return \Illuminate\Support\Collection
     */
    public function TicketInfo($ticket_id)
    {
        $ticket_info = Tickets::TicketsWithAllDetail()->where('ticket_id', $ticket_id)->first();
        $client_list = User::select(['user_id', 'first_name', 'last_name', 'department'])
            ->where('department', 2)->doesntHave('AssignedTicket')->get();
        $department_list = Department::AllDepartments()->get();
        if (!$ticket_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        $data = [
            'ticket' => $ticket_info,
            'users' => $client_list,
            'departments' => $department_list
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function UpdatePriority(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required|exists:tb_tickets,ticket_id',
            'priority_level' => 'required'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //update ticket info
        $ticket = Tickets::find($request->ticket_id);
        $ticket->priority_level = $request->priority_level;
        $ticket->Save();
        // save history
        $request['status'] = $ticket->status;
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Ticket']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function UpdateStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required|exists:tb_tickets,ticket_id',
            'status' => 'required'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //update ticket info
        $ticket = Tickets::find($request->ticket_id);
        $ticket->status = $request->status;
        $ticket->Save();
        // save history
        $request['priority_level'] = $ticket->priority_level;
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Ticket']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function AssignToAgent(Request $request)
    {
        $agent = AssignedTickets::where('assigned_to', $request->assigned_to)
            ->where('ticket_id', $request->ticket_id)->first();
        if ($agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $request['assigned_by'] = $request->user_id;
        $agent = AssignedTickets::create($request->all());
        if (!$agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Ticket']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function RemoveAgent(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_assigned_tickets,id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $remove_agent = AssignedTickets::find($request->id)->delete();
        if (!$remove_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Agent']));
    }
}
