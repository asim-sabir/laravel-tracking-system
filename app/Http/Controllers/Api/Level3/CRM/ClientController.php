<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\CountriesModel;
use App\Models\CustomerEmergencyContact;
use App\Models\CustomerVehicle;
use App\Models\OperationalTickets;
use App\Models\User;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function ClientList(Request $request)
    {
        $clients = Client::ClientBasicInfo()
            ->whereHas('Proposals.ProposalLead.Agents', function ($query) use ($request) {
                $query->where('assigned_to', $request->user_id);
            })->get();
        if (!count($clients))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $clients);
    }

    /**
     * @param Request $request
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function ClientInfo(Request $request, $client_id)
    {
        $client = Client::ClientInfoWithAllDetail()
            ->whereHas('Proposals.ProposalLead.Agents', function ($query) use ($request) {
                $query->where('assigned_to', $request->user_id);
            })->with(['FollowUpHistory.UserInfo' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name']);
            }, 'CustomerVehicles.ActiveLead' => function ($query) {
                $query->with(['LeadInfo' => function ($query) {
                    $query->select(['lead_id', 'proposal_id'])
                        ->with(['LeadProposal' => function ($query) {
                            $query->select(['proposal_id'])
                                ->with(['ProposalPackage' => function ($query) {
                                    $query->select(['proposal_id', 'customer_package_id', 'package_name'])
                                        ->with(['PackageFeatures' => function ($query) {
                                            $query->select(['feature_name', 'customer_package_id', 'is_free']);
                                        }]);
                                }]);
                        }]);
                }]);
            }, 'ActiveOperationalTicket' => function ($query) use ($request) {
                $query->select(['customer_id', 'operational_ticket_id', 'vehicle_id', 'type']);
                $query->whereHas('AssignedTo', function ($query) use ($request) {
                    $query->where('assigned_to', $request->user_id);
                });
            }])
            ->where('customer_id', $client_id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $client);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate(Request $request)
    {
        $opt_ticket = OperationalTickets::CheckTicketExist($request, 0, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        $client = Client::where('customer_id', $request->client_id)->first();

        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'client' => $client
        ];

        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new ClientRequest();
        $rules = $request_obj->rules();
        $rules['id'] = 'required|exists:tb_customer,customer_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $opt_ticket = OperationalTickets::CheckTicketExist($request, 0, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $client = Client::where('customer_id', $request->id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $client = $client->update($request->all());

        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        // close operational ticket
        $opt_ticket->status = 3;
        $opt_ticket->save();
        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Client']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetECUpdate(Request $request)
    {
        $opt_ticket = OperationalTickets::CheckTicketExist($request, 1, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $contact = CustomerEmergencyContact::where('customer_id', $request->client_id)->get();

        if (!count($contact))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        return HelperModule::jsonResponse(true, false, $contact);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostECUpdate(Request $request)
    {
        $opt_ticket = OperationalTickets::CheckTicketExist($request, 1, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $client = Client::where('customer_id', $request->id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        //remove emergency contacts
        CustomerEmergencyContact::where('customer_id', $request->id)->forceDelete();
        //add emergency contact
        for ($i = 0; $i < count($request->mobile_no); $i++) {
            if (isset($request->mobile_no[$i])) {
                if ($request->mobile_no[$i]) {
                    $client->EmergencyContact()->create([
                        'contact_person_name' => $request->contact_person_name[$i],
                        'contact_relation' => $request->contact_relation[$i],
                        'mobile_no' => $request->mobile_no[$i],
                        'phone_no' => $request->phone_no[$i]
                    ]);
                }
            }
        }
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        // close operational ticket
        $opt_ticket->status = 3;
        $opt_ticket->save();
        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Emergency contact']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetVEUpdate(Request $request)
    {
        $opt_ticket = OperationalTickets::CheckTicketExist($request, 2, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $vehicle = CustomerVehicle::BasicVehicleInfo()
            ->where('vehicle_id', $request->vehicle_id)->first();

        if (!count($vehicle))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'vehicle']));

        return HelperModule::jsonResponse(true, false, $vehicle);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostVEUpdate(Request $request)
    {
        $opt_ticket = OperationalTickets::CheckTicketExist($request, 2, 1)->first();
        if (!$opt_ticket)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $vehicle = CustomerVehicle::BasicVehicleInfo()->where('vehicle_id', $request->id)->first();
        if (!$vehicle)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'vehicle']));

        $vehicle = $vehicle->update($request->all());
        if (!$vehicle)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        // close operational ticket
        $opt_ticket->status = 3;
        $opt_ticket->save();
        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Vehicle']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_customer,customer_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = Client::GetClientByUserId($request->user_id)->where('customer_id', $request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Client']));
    }
}
