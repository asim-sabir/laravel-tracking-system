<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\TicketRequest;
use App\Models\AssignedTickets;
use App\Models\Client;
use App\Models\CustomerVehicle;
use App\Models\Department;
use App\Models\OperationalTickets;
use App\Models\Tickets;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class OperationalTicketsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function TicketList(Request $request)
    {
        $tickets = OperationalTickets::TicketsWithUserDetail('*', ['user_id', 'first_name', 'last_name'])
            ->with(['AssignedTo' => function ($query) {
                $query->select(['ticket_id', 'assigned_to']);
                $query->with(['AssignedUserInfo' => function ($user_query) {
                    $user_query->select(['user_id', 'first_name', 'last_name']);
                }]);
            }])
            ->where('created_by', $request->user_id)
            ->get();
        $assigned_tickets = OperationalTickets::TicketsWithUserDetail('*', ['user_id', 'first_name', 'last_name'])
            ->whereHas('AssignedTo', function ($query) use ($request) {
                $query->where('assigned_to', $request->user_id);
            })->get();
        if (!count($tickets) && !count($assigned_tickets))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'ticket record']));

        $data = [
            'tickets' => $tickets,
            'assigned_tickets' => $assigned_tickets,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd(Request $request)
    {
        $clients = Client::ClientBasicInfo(['customer_id', 'customer_name'])
            ->whereHas('Proposals.ProposalLead.Agents', function ($query) use ($request) {
                $query->where('assigned_to', $request->user_id);
            })
            ->get();
        $vehicles = CustomerVehicle::BasicVehicleInfo()
            ->whereHas('Customer.Proposals.ProposalLead.Agents', function ($query) use ($request) {
                $query->where('assigned_to', $request->user_id);
        })
            ->select(['vehicle_id', 'customer_id', 'plate_no'])
            ->get();
        if (!count($clients))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client record']));

        $data = [
            'clients' => $clients,
            'vehicles' => $vehicles,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new TicketRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //save ticket
        $request['created_by'] = $request->user_id;
        $request['operational_ticket_id'] = '';
        $request['status'] = 0;
        if($request->type >= 2)
            $request['vehicle_id'] = $request->vehicle;

        $ticket = OperationalTickets::create($request->all());
        // save status history
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Ticket']));
    }

    /**
     * @param $ticket_id
     * @return \Illuminate\Support\Collection
     */
    public function TicketInfo($ticket_id)
    {
        $ticket_info = OperationalTickets::TicketsWithAllDetail()->where('operational_ticket_id', $ticket_id)->first();
        if (!$ticket_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));
        $data = [
            'ticket' => $ticket_info,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function UpdatePriority(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required|exists:tb_operational_ticket,operational_ticket_id',
            'priority_level' => 'required'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //update ticket info
        $ticket = OperationalTickets::find($request->ticket_id);
        $ticket->priority_level = $request->priority_level;
        $ticket->Save();
        // save history
        $request['status'] = $ticket->status;
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Ticket']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function UpdateStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required|exists:tb_operational_ticket,operational_ticket_id',
            'status' => 'required'
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        //update ticket info
        $ticket = OperationalTickets::find($request->ticket_id);
        $ticket->status = $request->status;
        $ticket->Save();
        // save history
        $request['priority_level'] = $ticket->priority_level;
        $ticket_status = $ticket->StatusHistory()->create($request->all());
        if (!$ticket || !$ticket_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Ticket']));
    }
}
