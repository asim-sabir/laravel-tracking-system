<?php

namespace App\Http\Controllers\Api\Level3\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadFollowUpRequest;
use App\Models\LeadFollowUpHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class LeadFollowUpController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new LeadFollowUpRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $history = LeadFollowUpHistory::create($request->all());

        if (!$history)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Comment']));
    }
}
