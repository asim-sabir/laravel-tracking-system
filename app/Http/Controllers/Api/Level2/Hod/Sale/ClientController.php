<?php

namespace App\Http\Controllers\Api\Level2\Hod\Sale;

use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\CountriesModel;
use App\Models\CustomerAgents;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function ClientList(Request $request)
    {
        $clients = Client::ClientBasicInfo()->get();
        if (!count($clients))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $clients);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));

        $data = [
            'countries' => $countries,
            'cities' => $cities
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new ClientRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $request['user_id'] = $request->user_id;
        $request['customer_id'] = '';
        $client = Client::create($request->all());
        $client->Agents()->create([
            'user_id' => $request->user_id
        ]);

        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new client has been added');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Client']));
    }

    /**
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function ClientInfo($client_id)
    {
        $client = Client::ClientBasicInfo()
            ->with(['FollowUpHistory.UserInfo' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name']);
            }, 'ClientAgents' => function ($agent_query) {
                $agent_query->select(['tb_users.user_id', 'social_title', 'first_name', 'last_name']);
            }])
            ->where('customer_id', $client_id)->first();
        $agents = User::AllUsers()
            ->select(['user_id', 'social_title', 'first_name', 'last_name', 'department'])
            ->whereDoesntHave('CustomerAgents', function ($query) use ($client) {
                $query->where('customer_id', $client->customer_id);
            })
            ->where('department', 1)->get();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $data = [
            'client_info' => $client,
            'agents' => $agents
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function GetUpdate($client_id)
    {
        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        $client = Client::ClientBasicInfo()->where('customer_id', $client_id)->first();

        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'client' => $client
        ];

        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(Request $request)
    {
        $request_obj = new ClientRequest();
        $rules = $request_obj->rules();
        $rules['id'] = 'required|exists:tb_customer,customer_id';
        $validator = Validator::make($request->all(), $rules, $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $client = Client::GetClientByUserId($request->user_id)->where('customer_id', $request->id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'client']));

        $client = $client->update($request->all());

        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Client']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function Destroy(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_customer,customer_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = Client::ClientBasicInfo()->where('customer_id', $request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Client']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function AssignAgent(Request $request)
    {
        $customer_agent = CustomerAgents::where('user_id', $request->agent_id)
            ->where('customer_id', $request->customer_id)->first();
        if ($customer_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $customer_agent = CustomerAgents::create([
            'customer_id' => $request->customer_id,
            'user_id' => $request->agent_id
        ]);
        if (!$customer_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Agent']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function RemoveAgent(Request $request)
    {
        $data['id'] = $request->id;
        $data['customer_id'] = $request->customer_id;
        $validator = Validator::make($data, [
            'id' => 'required',
            'customer_id' => 'required',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = CustomerAgents::where('user_id', $request->id)->where('customer_id', $request->customer_id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Agent']));
    }
}
