<?php

namespace App\Http\Controllers\Api\Level2\Hod\Sale;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SaleController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function SaleList()
    {
        $sales = Sales::AllSaleWithDetail()->get();
        if (!count($sales))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $sales);
    }

    /**
     * @param $client_id
     * @return \Illuminate\Support\Collection
     */
    public function SaleInfo($client_id)
    {
        $client = Client::ClientBasicInfo()->where('customer_id', $client_id)->first();
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, false, $client);
    }

}
