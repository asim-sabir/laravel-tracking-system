<?php

namespace App\Http\Controllers\Api\Level2\Hod\Sale;

use App\HelperModules\HelperModule;
use App\HelperModules\NotificationModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use App\Http\Requests\LeadStatusRequest;
use App\Models\LeadAgents;
use App\Models\Leads;
use App\Models\LeadStatusHistory;
use App\Models\Proposal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function LeadList()
    {
        $leads = Leads::AllLeads()->get();
        if (!count($leads))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $leads);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function GetAdd()
    {
        $proposals = Proposal::BasicProposalInfo()->where('status', 0)->get();
        if (!count($proposals))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $proposals);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function store(Request $request)
    {
        $request_obj = new LeadRequest();
        $validator = Validator::make($request->all(), $request_obj->rules(), $request_obj->messages());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        // get proposal info
        $proposal = Proposal::where('proposal_id', $request->proposal_id)->first();
        if (!$proposal)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        $proposal->status = 1;
        $proposal->save();

        //save lead
        $request['user_id'] = $request->user_id;
        $request['lead_id'] = '';
        $lead = $proposal->ProposalLead()->create($request->all());
        //save lead status history
        $lead_status = $lead->LeadStatusHistory()->create($request->all());
        //save lead follow up history
        if ($request->filled('comment'))
            $lead->LeadFollowUpHistory()->create($request->all());

        if (!$proposal || !$lead || !$lead_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.create', ['attribute' => 'Lead']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function PutStatusUpdate(Request $request)
    {
        $request_obj = new LeadStatusRequest();
        $validator = Validator::make($request->all(), $request_obj->rules());
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());
        //update lead status
        Leads::find($request->lead_id)->update(['status' => $request->status]);
        //save lead status history
        $request['user_id'] = $request->user_id;
        $lead_status = LeadStatusHistory::create($request->all());

        if (!$lead_status)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        //push notification
//        NotificationModule::Notification($request, 'A new lead has been created');

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'status']));
    }
    /**
     * @param $lead_id
     * @return \Illuminate\Support\Collection
     */
    public function LeadInfo($lead_id)
    {
        $user_select = ['user_id', 'first_name', 'last_name'];
        $lead = Leads::AllLeadsWithDetail('*', $user_select)
            ->with(['Agents.AssignedTo' => function($query){
                $query->select(['user_id', 'first_name', 'last_name']);
            }])
            ->where('lead_id', $lead_id)->first();
        $agents = User::AllUsers()
            ->select(['user_id', 'social_title', 'first_name', 'last_name', 'department'])
            ->whereDoesntHave('UserLeads', function ($query) use ($lead) {
                $query->where('lead_id', $lead->lead_id);
            })
            ->where('department', 1)->get();
        if (!$lead)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $data = [
            'lead' => $lead,
            'agents' => $agents
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function AssignAgent(Request $request)
    {
        $lead_agent = LeadAgents::where('assigned_to', $request->agent_id)
            ->where('lead_id', $request->lead_id)->first();
        if ($lead_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $lead_agent = LeadAgents::create([
            'lead_id' => $request->lead_id,
            'assigned_by' => $request->user_id,
            'assigned_to' => $request->agent_id,
        ]);
        if (!$lead_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Agent']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function RemoveAgent(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_lead_agents,lead_agent_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = LeadAgents::find($request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Agent']));
    }

}
