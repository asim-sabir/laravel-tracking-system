<?php

namespace App\Http\Controllers\Api\Level2\Hod\Sale;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AgentsController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function AgentList()
    {
        $agents = User::AllUsers()
            ->where('department', 1)
            ->where('user_id', '<>', Auth::user()->user_id)->get();
        if (!count($agents))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'record']));

        return HelperModule::jsonResponse(true, false, $agents);
    }

    /**
     * @param $agent_id
     * @return \Illuminate\Support\Collection
     */
    public function AgentInfo($agent_id)
    {
        $agent_info = User::with('UserClients')
            ->where('department', 1)
            ->where('user_id', $agent_id)
            ->first();
        $customers = Client::ClientBasicInfo()->doesntHave('ClientAgents')->get();
        if (!$agent_info)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $data = [
            'agents' => $agent_info,
            'customers' => $customers
        ];

        return HelperModule::jsonResponse(true, false, $data);
    }
}
