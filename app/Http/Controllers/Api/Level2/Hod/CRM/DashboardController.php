<?php

namespace App\Http\Controllers\Api\Level2\Hod\CRM;

use App\HelperModules\CalculationModule;
use App\HelperModules\DateTimeModule;
use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Leads;
use App\Models\Notification;
use App\Models\Proposal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetHeaderInfo(Request $request)
    {
        $notification = Notification::BasicLeadInfo()
            ->where('created_by', $request->user_id)
            ->orWhere('dept_id', 2)
            ->orWhere('job_post', 'hod')->get();
        $total_clients = Client::GetClientByUserId($request->user_id)->groupBy('customer_id')->count();
        $total_sales = Leads::BasicLeadInfo()->where('user_id', $request->user_id)->where('status', 2)
            ->groupBy('lead_id')->count();
        $total_leads = Leads::BasicLeadInfo()
            ->where('user_id', $request->user_id)
            ->groupBy('lead_id')
            ->count();
        $data = [
            'total_clients' => $total_clients,
            'total_sales' => $total_sales,
            'total_leads' => $total_leads,
            'notification' => $notification,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetInfo(Request $request)
    {
        $notification = Notification::BasicLeadInfo()
            ->where('created_by', $request->user_id)
            ->orWhere('dept_id', 2)
            ->orWhere('job_post', 'hod')
            ->get();
        $sales = Leads::BasicLeadInfo(['user_id', 'status', DB::raw('COUNT(lead_id) as lead_counter'), DB::raw('FORMAT(created_at, \'Y-m-d\') as format_create_date')])
            ->where(function ($query) use ($request) {
                $query->where('user_id', $request->user_id)->where('status', 2);
                if ($request->start_date) {
                    $query->where(DB::raw('FORMAT(created_at, \'Y-m-d\')'), '>=', $request->start_date);
                    if ($request->end_date) {
                        $query->where(DB::raw('FORMAT(created_at, \'Y-m-d\')'), '<=', $request->end_date);
                    }
                } else {
                    $query->where(DB::raw('FORMAT(created_at, \'Y-m-d\')'), '>=', DateTimeModule::TimeFormat(Carbon::now()->subDays(30), 'Y-m-d'));
                }
            })
            ->groupBy(['user_id', 'lead_id', 'status', 'created_at'])
            ->get();
        $total_clients = Client::GetClientByUserId($request->user_id)->groupBy('customer_id')->count();
        $leads = Leads::AllLeads()->where('user_id', $request->user_id)->get();
        $total_leads = $leads->count();
        $total_sales = $leads->where('status', 2)->count();
        $proposals = Proposal::where('user_id', $request->user_id)->get();
        $total_proposals = $proposals->count();
        $approved_proposals = $proposals->where('status', 1)->count();
        $total_user_sales = $total_sales;
        $total_sales = CalculationModule::PercentagesCalculation($total_sales, $total_clients);
        $total_leads = CalculationModule::PercentagesCalculation($total_leads, $total_clients);
        $total_clients = 100 - ($total_sales + $total_leads);
        if (!$total_sales && !$total_leads)
            $total_clients = 0;
        $data = [
            'monthly_sale' => $sales,
            'total_clients' => $total_clients,
            'total_sales' => $total_sales,
            'total_leads' => $total_leads,
            'total_proposals' => $total_proposals,
            'approved_proposals' => $approved_proposals,
            'total_user_sales' => $total_user_sales,
            'notification' => $notification,
            'leads' => $leads,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }
}
