<?php

namespace App\Http\Controllers\Api\Level2\Hod\CRM;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\Level3\CRM\LeadRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\CountriesModel;
use App\Models\CustomerEmergencyContact;
use App\Models\CustomerVehicle;
use App\Models\LeadAgents;
use App\Models\Leads;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function LeadList(Request $request)
    {
        $leads = Leads::BasicLeadInfo($request->user_id)
            ->with(['Agents.AssignedTo' => function ($query) {
                $query->select(['user_id', 'social_title', 'first_name', 'last_name']);
            }, 'LeadProposal' => function ($query) {
                $query->select(['proposal_id', 'customer_id'])
                    ->with(['ClientInfo' => function ($client_query) {
                        $client_query->select(['customer_id', 'social_title', 'customer_name']);
                    }]);
            }])
            ->select(['lead_id', 'proposal_id', 'status', 'created_at'])
            ->where('status', 2)
            ->get();

        if (!count($leads))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'lead']));

        return HelperModule::jsonResponse(true, false, $leads);
    }

    /**
     * @param Request $request
     * @param $lead_id
     * @return \Illuminate\Support\Collection
     */
    public function LeadInfo(Request $request, $lead_id)
    {
        $lead = Leads::BasicLeadInfo()
            ->with(['LeadProposal' => function ($query) {
                $query->select(['proposal_id', 'customer_id']);
                $query->with('ClientInfo.EmergencyContact');
            }, 'LeadVehicle' => function ($query) {
                $query->with('VehicleInfo')
                    ->select(['vehicle_id', 'lead_id', 'status'])
                    ->where('status', 1);
            }])
            ->select(['lead_id', 'proposal_id', 'status', 'created_at'])
            ->where('lead_id', $lead_id)
            ->first();
        $countries = CountriesModel::AllCountries()->get();
        $cities = City::AllCities()->get();
        if (!count($countries))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'countries']));
        if (!count($cities))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'cities']));
        if (!$lead)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'lead']));

        $data = [
            'lead' => $lead,
            'countries' => $countries,
            'cities' => $cities
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param LeadRequest $request
     * @return \Illuminate\Support\Collection
     */
    public function PostUpdate(LeadRequest $request)
    {
        $lead = Leads::BasicLeadInfo()
            ->with(['LeadProposal' => function ($query) {
                $query->select(['proposal_id', 'customer_id']);
                $query->with('ClientInfo');
            }])
            ->where('lead_id', $request->id)->first();
        if (!$lead)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'lead']));
        $customer_id = $lead->LeadProposal->ClientInfo->customer_id;
        //update client info
        $client = Client::find($customer_id);
        $client->update($request->all());
        //remove emergency contacts
        CustomerEmergencyContact::where('customer_id', $customer_id)->forceDelete();
        //remove lead vehicle
        CustomerVehicle::GetVehicleByLeadId($lead->lead_id)->where('customer_id', $customer_id)->forceDelete();
        //add emergency contact
        for ($i = 0; $i < count($request->mobile_no); $i++) {
            if (isset($request->mobile_no[$i])) {
                if ($request->mobile_no[$i]) {
                    $client->EmergencyContact()->create([
                        'contact_person_name' => $request->contact_person_name[$i],
                        'contact_relation' => $request->contact_relation[$i],
                        'mobile_no' => $request->mobile_no[$i],
                        'phone_no' => $request->phone_no[$i]
                    ]);
                }
            }
        }
        //vehicle information
        $vehicle_data = [];
        for ($i = 0; $i < count($request->plate_no); $i++) {
            if (isset($request->plate_no[$i])) {
                if ($request->plate_no[$i]) {
                    $vehicle_data [] =
                    $vehicle = $client->CustomerVehicles()->create([
                        'vehicle_id' => $i,
                        'plate_no' => $request->plate_no[$i],
                        'year_of_manufacturing' => $request->year_of_manufacturing[$i],
                        'vehicle_make' => $request->vehicle_make[$i],
                        'vehicle_model' => $request->vehicle_model[$i],
                        'vehicle_color' => $request->vehicle_color[$i],
                        'vehicle_engine_cc' => $request->vehicle_engine_cc[$i],
                        'vehicle_engine_no' => $request->vehicle_engine_no[$i],
                        'vehicle_chassis_no' => $request->vehicle_chassis_no[$i],
                        'fuel_type' => $request->fuel_type[$i] ? $request->fuel_type[$i] : 0,
                        'transmission_type' => $request->transmission_type[$i] ? $request->transmission_type[$i] : 0
                    ]);
                    //save lead history
                    $vehicle->LeadHistory()->create([
                        'lead_id' => $lead->lead_id
                    ]);
                }
            }
        }
        if (!$client)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Lead']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetAgents(Request $request)
    {
        $agents = User::select(['user_id', 'social_title', 'first_name', 'last_name', 'department'])
            ->whereDoesntHave('UserLeads', function ($query) use ($request) {
                $query->where('lead_id', $request->lead_id);
            })
            ->where('department', 2)->get();
        if (!count($agents))
            return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'agent']));
        $data = [
            'agents' => $agents,
        ];
        return HelperModule::jsonResponse(true, false, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function AssignAgent(Request $request)
    {
        $lead_agent = LeadAgents::where('assigned_to', $request->agent_id)
            ->where('lead_id', $request->lead_id)->first();
        if ($lead_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        $lead_agent = LeadAgents::create([
            'lead_id' => $request->lead_id,
            'assigned_by' => $request->user_id,
            'assigned_to' => $request->agent_id,
        ]);
        if (!$lead_agent)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.update', ['attribute' => 'Agent']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function RemoveAgent(Request $request)
    {
        $data['id'] = $request->id;
        $validator = Validator::make($data, [
            'id' => 'required|exists:tb_lead_agents,lead_agent_id',
        ]);
        if ($validator->fails())
            return HelperModule::jsonResponse(false, $validator->errors()->first());

        $user = LeadAgents::find($request->id)->delete();
        if (!$user)
            return HelperModule::jsonResponse(false, Lang::get('messages.error.general'));

        return HelperModule::jsonResponse(true, Lang::get('messages.success.destroy', ['attribute' => 'Agent']));
    }
}
