<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'priority_level' => 'required',
            'dept_id' => 'required',
            'customer_id' => 'required|exists:tb_customer,customer_id',
            'ticket_title' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'customer_id.unique' => Lang::get('validation.unique', ['attribute' => 'client'])
        ];
    }
}
