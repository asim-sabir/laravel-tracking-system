<?php

namespace App\Http\Requests\Level3\CRM;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:tb_leads,lead_id',
            'customer_name' => 'required',
            'city' => 'required',
            'country' => 'required',
            'mobile_1' => 'required',
        ];
    }
}
