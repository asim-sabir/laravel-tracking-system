<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'product_model' => 'required',
            'company_id' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => Lang::get('validation.required', ['attribute' => 'product id']),
            'product_name.required' => Lang::get('validation.required', ['attribute' => 'product name']),
            'product_model.required' => Lang::get('validation.required', ['attribute' => 'product model']),
            'company_id.required' => Lang::get('validation.required', ['attribute' => 'company']),
        ];
    }
}
