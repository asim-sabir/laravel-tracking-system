<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proposal_id' => 'required|exists:tb_proposal,proposal_id|unique:tb_leads,proposal_id'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'proposal_id.unique' => Lang::get('validation.unique', ['attribute' => 'lead'])
        ];
    }
}
