<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'customer_id' => 'required|exists:tb_customer,customer_id',
            'package_id' => 'required|exists:tb_packages,package_id'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => Lang::get('validation.required', ['attribute' => 'proposal']),
            'customer_id.required' => Lang::get('validation.required', ['attribute' => 'customer']),
            'package_id.required' => Lang::get('validation.required', ['attribute' => 'package'])
        ];
    }
}
