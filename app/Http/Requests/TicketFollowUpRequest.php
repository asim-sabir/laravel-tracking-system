<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class TicketFollowUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_id' => 'required|exists:tb_tickets,ticket_id',
            'customer_id' => 'required',
            'commented_by' => 'required',
            'comment' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'commented_by.required' => Lang::get('validation.required', ['attribute' => 'user'])
        ];
    }
}
