<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class FeatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'feature_name' => 'required|unique:tb_features,feature_name',
            'feature_sale_price' => 'required|numeric',
            'feature_cost_price' => 'required|numeric',
            'feature_type' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'feature_id.required' => Lang::get('validation.required', ['attribute' => 'feature id']),
            'feature_name.required' => Lang::get('validation.required', ['attribute' => 'feature name']),
            'feature_sale_price.required' => Lang::get('validation.required', ['attribute' => 'feature sale price']),
            'feature_cost_price.required' => Lang::get('validation.required', ['attribute' => 'feature cost price']),
            'feature_type.required' => Lang::get('validation.required', ['attribute' => 'feature type']),
        ];
    }
}
