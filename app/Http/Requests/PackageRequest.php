<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_name' => 'required|unique:tb_packages,package_name',
            'product_id' => 'required',
            'feature_id.*' => 'required',
            'is_free.*' => 'required',
            'package_min_cost' => 'required|numeric',
            'package_sale_price' => 'required|numeric'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => Lang::get('validation.required', ['attribute' => 'product id']),
            'package_name.required' => Lang::get('validation.required', ['attribute' => 'package name'])
        ];
    }
}
