<?php

namespace App\HelperModules;

use App\Events\ClientNotification;

/* @author <khakan@redsignal.biz> */
class NotificationModule
{
    static public function ClientNotification($data, $message, $post='other')
    {
        $notification_data = [
            'created_by' => $data->user_id,
            'dept_id' => $data->department,
            'job_post' => $post,
            'message' => $message,
            'type' => 0,
            'is_seen' => 0
        ];
        return event(new ClientNotification($notification_data));
    }
}