<?php

namespace App\HelperModules;

use App\Events\PushEmail;
use App\Models\EmailSetting;
use App\Models\User;

class MailSender
{
    /**
     * @param $user
     * @param $template_view
     * @param $data
     * @param $type
     * @return array|null
     */
    static public function PushMail($user, $template_view, $data, $type)
    {
        return event(new PushEmail($user, $template_view, $data, $type));
    }

    /**
     * @param $order
     * @param $mail_to_sent
     * @param $type
     */
    static public function OrderEmail($order, $mail_to_sent, $type){
        $user = new User();
        $email_address = EmailSetting::AllEmails()->whereIn('email_id', $mail_to_sent)->get();
        foreach ($email_address as $email){
            $user->email = $email->email_address;
            self::PushMail($user, 'mail-template.order', $order, $type.'.'.$order->order_id);
        }
    }

    /**
     * @param $data
     * @return mixed|string
     */
    static public function ExplodeType($data)
    {
        $explode_data = explode('.', $data);
        if(count($explode_data) > 1)
            return config('mail.subject.' . $explode_data[0]) . ' ' . $explode_data[1];

        return config('mail.subject.' . $explode_data[0]);
    }
}