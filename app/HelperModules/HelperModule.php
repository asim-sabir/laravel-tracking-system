<?php

namespace App\HelperModules;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/* @author <khakan@redsignal.biz> */
class HelperModule
{
    /**
     * @param $type
     * @param $message
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    public static function jsonResponse($type, $message = false, $data = null)
    {
        $response['isResponse'] = $type;
        if ($message)
            $response['message'] = $message;
        if ($data)
            $response['data'] = $data;

        return collect($response);
    }

    /**
     * @param $type
     * @param $message
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    public static function jsonApiResponse($type, $message = null, $data = null)
    {
        $response['status'] = $type;
        $response['message'] = $message;
        $response['data'] = $data;

        return collect($response);
    }

    /**
     * @return string
     */
    static public function generateToken()
    {
        return hash_hmac('sha256', str_random(10), config('app.key'));
    }

    /**
     * @return mixed
     */
    static public function PageTitle()
    {
        $path = \Request::path();
        $path = explode('/', $path);
        if (count($path) > 3)
            unset($path[count($path) - 1]);
        $path = implode('.', $path);
        if (config('titles.' . $path)) {
            $title = config('titles.' . $path);
            if (is_array($title))
                return $title['/'];
            return $title;
        }

        return config('titles.default');
    }

    /**
     * @param $data
     * @return string
     */
    static public function HashMd5($data)
    {
        $params = [];
        foreach ($data as $key => $val) {
            $params[$key] = md5($val);
        }
        return urlencode(http_build_query($params));
    }

    /**
     * @param $data
     * @return string
     */
    static public function HashMd5Salt($data)
    {
        $secKey = static::secretKey();
        $params = [];
        foreach ($data as $key => $val) {
            $params[$key] = md5($secKey . $val);
        }
        //dd(http_build_query($params));
        //return urlencode(http_build_query($params));
        return http_build_query($params);
    }

    /**
     * @param $data
     * @return string
     */
    static public function QRCode($data)
    {
        $url = self::HashMd5($data);
        $image = "http://chart.apis.google.com/chart?cht=qr&chs=200x200&chld=H|0&chl=" . $url;
        return '<img src=' . $image . ' alt="QR Code" />';
    }

    /**
     * @return string
     * Return Secret Key Being used in Salt for sms & E-mail link to ticket
     * Created by Abdullah Butt
     */
    static public function secretKey()
    {
        return "(*&JKHUKG(&*H";
    }

    /**
     * @param $input
     * @return string
     * Return Order and Ticket id with 6 digits code by adding leading zeroes to make length of Order or Ticket 6 digits.... e.g order id 6 will return 000006
     * Created by Abdullah Butt
     */
    static public function sixDigitCode($input)
    {
        return sprintf('%06d', $input);
        //$order = sprintf('%06d', $input);
        //$abc = str_pad($input, 6, '0', STR_PAD_LEFT);

    }

    /**
     * @param $quantity
     * @param $price
     * @return mixed
     */
    static public function PriceCalculation($quantity, $price)
    {
        return $quantity * $price;
    }

    /**
     * @param $data
     * @param $sorting_field
     * @param $request
     * @return array
     */
    static public function DataResponseForSoringPlugin($data, $sorting_field, $request)
    {
        $data_table = !empty($request->datatable) ? $request->datatable : array();
        $data_table = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $data_table);
        // search filter by keywords
        $filter = isset($data_table['query']['generalSearch']) && is_string($data_table['query']['generalSearch']) ? $data_table['query']['generalSearch'] : '';
        if (!empty($filter)) {
            $data = array_filter($data, function ($a) use ($filter) {
                return (boolean)preg_grep("/$filter/i", (array)$a);
            });
            unset($data_table['query']['generalSearch']);
        }
        // filter by field query
        $query = isset($data_table['query']) && is_array($data_table['query']) ? $data_table['query'] : null;
        if (is_array($query)) {
            $util_obj = new List_Util($data);
            $query = array_filter($query);
            foreach ($query as $key => $val) {
                $data = $util_obj->list_filter($data, array($key => $val));
            }
        }
        $sort = !empty($data_table['sort']['sort']) ? $data_table['sort']['sort'] : 'asc';
        $field = !empty($data_table['sort']['field']) ? $data_table['sort']['field'] : $sorting_field;

        $meta = array();
        $page = !empty($data_table['pagination']['page']) ? (int)$data_table['pagination']['page'] : 1;
        $perpage = !empty($data_table['pagination']['perpage']) ? (int)$data_table['pagination']['perpage'] : -1;

        $pages = 1;
        $total = count($data); // total items in array
        // $perpage 0; get all data
        if ($perpage > 0) {
            $pages = ceil($total / $perpage); // calculate total pages
            $page = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
            $page = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ($page - 1) * $perpage;
            if ($offset < 0) {
                $offset = 0;
            }
            $data = array_slice($data, $offset, $perpage, true);
        }
        $meta = array(
            'page' => $page,
            'pages' => $pages,
            'perpage' => $perpage,
            'total' => $total,
        );
        $result = array(
            'meta' => $meta + array(
                    'sort' => $sort,
                    'field' => $field,
                ),
            'data' => $data,
        );
        return $result;
    }

    /**
     * @param $data
     * @return float
     */
    static public function NoOfPages($data)
    {
        return ceil(count($data) / config('app.paginate'));
    }

    /**
     * @param $prev
     * @param $current
     * @return float|int
     */
    static public function GrowthCalculation($prev, $current)
    {
        return number_format((($current - $prev)/$prev) * 100, 0);
    }

    /**
     * @param string $prefix
     * @return string
     */
    static public function CustomID($prefix = 'CX')
    {
        $current_date = Carbon::now()->format('Ymd');
        $strToTime = substr(DateTimeModule::StrToTime(), -4);
        $id = $current_date . $prefix . $strToTime;
        return $id;
    }
}