<?php

namespace App\HelperModules;


/* @author <khakan@redsignal.biz> */
class CalculationModule
{
    /**
     * @param $prev
     * @param $current
     * @return float|int
     */
    static public function GrowthCalculation($prev, $current)
    {
        return number_format((($current - $prev) / $prev) * 100, 0);
    }

    /**
     * @param $val1
     * @param $val2
     * @return float|int
     */
    static public function PercentagesCalculation($val1, $val2)
    {
        if(!$val1 || !$val2)
            return 0;
        return ($val1 / $val2) * 100;
    }

    /**
     * @param $val1
     * @param $val2
     * @return float
     */
    static public function CalculateTax($val1, $val2)
    {
        return round(($val1/100) * $val2);
    }
}