<?php

namespace App\HelperModules;


use Illuminate\Support\Facades\Storage;

class FileModules
{

    /**
     * @param $file
     * @param $path
     * @param $folderName
     * @param string $base_folder
     * @return mixed
     */
    static public function FileUpload($file, $path, $folderName, $base_folder = 'uploads/')
    {
        $path = $path . $folderName;
        $destination_folder = base_path('public/' . $base_folder . $path . '/');
        self::make_dir($destination_folder, 0777, true);
        $image_name = self::RandomFileName($file, $folderName);
        $file->move($destination_folder, $image_name);
        return $image_name;
    }

    static public function FileUploadToStorage($file, $path, $driver)
    {
        $storage = Storage::disk($driver)->put($path, $file);
        return $storage;
    }

    /** -- Make directory --
     * @param $path
     * @param $permission
     * @param bool $recursive
     * @return bool
     */
    static public function make_dir($path, $permission, $recursive= false)
    {
        if (file_exists($path))
            return true;
        return mkdir($path, $permission, $recursive);
    }

    /**
     * @param $file
     * @return string
     */
    static public function RandomFileName($file, $name)
    {
        $type = $file->getClientOriginalExtension();
        return $name . '-' . DateTimeModule::TimeInMilliSec() . '.' . $type;
    }

    /**
     * @param $path
     * @return string
     */
    static public function GetFilePath($path)
    {
        return asset($path);
    }
}