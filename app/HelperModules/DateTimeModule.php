<?php

namespace App\HelperModules;
use Carbon\Carbon;

/* @author <khakan@redsignal.biz> */
class DateTimeModule
{
    /**
     * @param $time
     * @param string $format
     * @return string
     */
    static public function TimeFormat($time, $format = 'Y-m-d G:i:s')
    {
        return Carbon::createFromFormat('Y-m-d G:i:s', self::CarbonObject($time)->toDateTimeString())->format($format);
    }

    /**
     * @return DateTimeModule|Carbon
     */
    static public function CurrentTime()
    {
        return self::TimeFormat(self::CurrentDateTime());
    }

    /**
     * @return DateTimeModule|Carbon
     */
    static public function CurrentDateTime()
    {
        return Carbon::now();
    }
    /**
     * @param $data
     * @return DateTimeModule|Carbon
     */
    static public function CarbonObject($data)
    {
        return Carbon::parse($data);
    }

    /**
     * @param $second_date
     * @return mixed
     */
    static public function DiffForHumans($second_date)
    {
        $current_date = self::CurrentDateTime();
        $second_date = self::CarbonObject($second_date);
        return $second_date->diffForHumans($current_date,  true);
    }
    /**
     * @param $date
     * @return false|int
     */
    static public function StrToTime($date = null)
    {
        if(!$date)
            $date = Carbon::now();
        return strtotime($date);
    }

    static public function TimeInMilliSec()
    {
        return round(microtime(true) * 1000);
    }
}