<?php

namespace App\Listeners;

use App\Events\LeadEventNotification;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeadEventNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LeadEventNotification  $event
     * @return void
     */
    public function handle(LeadEventNotification $event)
    {
//        Notification::create($event->data);
    }
}
