<?php

namespace App\Listeners;

use App\Events\ClientNotification;
use App\Models\Notification;

class ClientNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ClientNotification $event)
    {
        Notification::create($event->data);
    }
}
