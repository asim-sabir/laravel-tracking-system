<?php

return [
    'success' => [
        'create' => ':attribute information saved successfully',
        'update' => ':attribute information updated successfully',
        'destroy' => ':attribute information deleted successfully',
        'not_found' => ':name not found',
    ],
    'error' => [
        'not_found' => 'No :attribute found',
        'general' => 'Oops!something is wrong. Try later',
        'select' => 'Nothing is selected',
        'permission' => 'You don\'t have permission to perform this action.',
        'assign' => 'Please assign brands to this category first.',
        'coupon_expire' => 'Coupon has been expired',
        'tcs_api' => 'Failed to book shipment'
    ]
];