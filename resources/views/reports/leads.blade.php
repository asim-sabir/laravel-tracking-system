<table>
    <thead>
    <tr>
        <th>Lead ID</th>
        <th>Proposal ID</th>
        <th>Client Name</th>
        <th>Proposal</th>
        <th>Mobile Number</th>
        <th>Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($leads as $lead)
        <tr>
            <td>{{ $lead->lead_id }}</td>
            <td>{{ $lead->LeadProposal->proposal_id }}</td>
            <td>{{ $lead->LeadProposal->ClientInfo->customer_name }}</td>
            <td>{{ $lead->LeadProposal->name }}</td>
            <td>{{ $lead->LeadProposal->ClientInfo->mobile_1 }}</td>
            <td>{{ $lead->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
