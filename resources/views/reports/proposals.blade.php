<table>
    <thead>
    <tr>
        <th>Proposal ID</th>
        <th>Client ID</th>
        <th>Client Name</th>
        <th>Proposal</th>
        <th>Mobile Number</th>
        <th>Created At</th>
        <th>Status</th>
        <th>Type</th>
    </tr>
    </thead>
    <tbody>
    @foreach($proposals as $proposal)
        <tr>
            <td>{{ $proposal->proposal_id }}</td>
            <td>{{ $proposal->customer_id }}</td>
            <td>{{ $proposal->ClientInfo->customer_name }}</td>
            <td>{{ $proposal->name }}</td>
            <td>{{ $proposal->ClientInfo->mobile_1 }}</td>
            <td>{{ $proposal->created_at }}</td>
            <td>{{ $proposal->status == 0?'In progress':'Lead Generated'  }}</td>
            <td>{{ $proposal->ClientInfo->customer_type == 0?'Individual':'corporate' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
