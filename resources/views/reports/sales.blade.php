<table>
    <thead>
    <tr>
        <th>Sale ID</th>
        <th>Lead ID</th>
        <th>Client Name</th>
        <th>Proposal</th>
        <th>Mobile Number</th>
        <th>Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as $sale)
        <tr>
            <td>{{ $sale->sale_id }}</td>
            <td>{{ $sale->Lead->lead_id }}</td>
            <td>{{ $sale->Lead->LeadProposal->ClientInfo->customer_name }}</td>
            <td>{{ $sale->Lead->LeadProposal->name }}</td>
            <td>{{ $sale->Lead->LeadProposal->ClientInfo->mobile_1 }}</td>
            <td>{{ $sale->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
