<table>
    <thead>
    <tr>
        <th>Client ID</th>
        <th>Name</th>
        <th>Mobile Number</th>
        <th>Status</th>
        <th>Type</th>
    </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        <tr>
            <td>{{ $client->customer_id }}</td>
            <td>{{ $client->customer_name }}</td>
            <td>{{ $client->mobile_1 }}</td>
            <td>{{ $client->status == 0?'Not Active':'Active'  }}</td>
            <td>{{ $client->customer_type == 0?'Individual':'corporate' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
