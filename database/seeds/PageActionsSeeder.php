<?php

use App\Models\PageActions;
use Illuminate\Database\Seeder;

class PageActionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /**-- level 2 hod sale--*/
            [
                'page_id' => 7,
                'action' => 'view'
            ],[
                'page_id' => 7,
                'action' => 'update'
            ],[
                'page_id' => 8,
                'action' => 'view'
            ],[
                'page_id' => 9,
                'action' => 'list'
            ],[
                'page_id' => 9,
                'action' => 'add'
            ],[
                'page_id' => 9,
                'action' => 'view'
            ],[
                'page_id' => 9,
                'action' => 'update'
            ],[
                'page_id' => 9,
                'action' => 'delete'
            ],[
                'page_id' => 10,
                'action' => 'add'
            ],[
                'page_id' => 11,
                'action' => 'list'
            ],[
                'page_id' => 11,
                'action' => 'add'
            ],[
                'page_id' => 11,
                'action' => 'view'
            ],[
                'page_id' => 11,
                'action' => 'delete'
            ],[
                'page_id' => 12,
                'action' => 'list'
            ],[
                'page_id' => 12,
                'action' => 'add'
            ],[
                'page_id' => 12,
                'action' => 'view'
            ],[
                'page_id' => 12,
                'action' => 'update'
            ],[
                'page_id' => 12,
                'action' => 'delete'
            ],[
                'page_id' => 13,
                'action' => 'add'
            ],[
                'page_id' => 14,
                'action' => 'list'
            ],[
                'page_id' => 14,
                'action' => 'view'
            ],[
                'page_id' => 15,
                'action' => 'view'
            ],[
                'page_id' => 16,
                'action' => 'list'
            ],[
                'page_id' => 16,
                'action' => 'view'
            ],[
                'page_id' => 17,
                'action' => 'list'
            ],[
                'page_id' => 17,
                'action' => 'add'
            ],[
                'page_id' => 17,
                'action' => 'view'
            ],[
                'page_id' => 17,
                'action' => 'update'
            ],[
                'page_id' => 18,
                'action' => 'add'
            ],
            /**-- level 2 hod crm--*/
            [
                'page_id' => 19,
                'action' => 'view'
            ],[
                'page_id' => 19,
                'action' => 'update'
            ],[
                'page_id' => 20,
                'action' => 'view'
            ],[
                'page_id' => 21,
                'action' => 'list'
            ],[
                'page_id' => 21,
                'action' => 'view'
            ],[
                'page_id' => 22,
                'action' => 'add'
            ],[
                'page_id' => 23,
                'action' => 'update'
            ],[
                'page_id' => 24,
                'action' => 'update'
            ],[
                'page_id' => 25,
                'action' => 'update'
            ],[
                'page_id' => 26,
                'action' => 'list'
            ],[
                'page_id' => 26,
                'action' => 'update'
            ],[
                'page_id' => 27,
                'action' => 'add'
            ],[
                'page_id' => 28,
                'action' => 'list'
            ],[
                'page_id' => 28,
                'action' => 'add'
            ],[
                'page_id' => 28,
                'action' => 'view'
            ],[
                'page_id' => 28,
                'action' => 'update'
            ],[
                'page_id' => 29,
                'action' => 'list'
            ],[
                'page_id' => 29,
                'action' => 'add'
            ],[
                'page_id' => 29,
                'action' => 'view'
            ],[
                'page_id' => 29,
                'action' => 'update'
            ],[
                'page_id' => 30,
                'action' => 'add'
            ],
            /**-- level 3 sale--*/
            [
                'page_id' => 31,
                'action' => 'view'
            ],[
                'page_id' => 31,
                'action' => 'update'
            ],[
                'page_id' => 32,
                'action' => 'view'
            ],[
                'page_id' => 33,
                'action' => 'list'
            ],[
                'page_id' => 33,
                'action' => 'add'
            ],[
                'page_id' => 33,
                'action' => 'view'
            ],[
                'page_id' => 33,
                'action' => 'update'
            ],[
                'page_id' => 33,
                'action' => 'delete'
            ],[
                'page_id' => 34,
                'action' => 'add'
            ],[
                'page_id' => 35,
                'action' => 'list'
            ],[
                'page_id' => 35,
                'action' => 'add'
            ],[
                'page_id' => 35,
                'action' => 'view'
            ],[
                'page_id' => 35,
                'action' => 'delete'
            ],[
                'page_id' => 36,
                'action' => 'list'
            ],[
                'page_id' => 36,
                'action' => 'add'
            ],[
                'page_id' => 36,
                'action' => 'view'
            ],[
                'page_id' => 37,
                'action' => 'add'
            ],[
                'page_id' => 38,
                'action' => 'list'
            ],[
                'page_id' => 38,
                'action' => 'view'
            ],[
                'page_id' => 39,
                'action' => 'view'
            ],[
                'page_id' => 40,
                'action' => 'list'
            ],[
                'page_id' => 40,
                'action' => 'add'
            ],[
                'page_id' => 40,
                'action' => 'view'
            ],[
                'page_id' => 40,
                'action' => 'update'
            ],[
                'page_id' => 41,
                'action' => 'list'
            ],[
                'page_id' => 41,
                'action' => 'add'
            ],[
                'page_id' => 41,
                'action' => 'view'
            ],[
                'page_id' => 41,
                'action' => 'update'
            ],[
                'page_id' => 42,
                'action' => 'add'
            ],
            /**-- level 3 crm --*/
            [
                'page_id' => 43,
                'action' => 'view'
            ],[
                'page_id' => 43,
                'action' => 'update'
            ],[
                'page_id' => 44,
                'action' => 'view'
            ],[
                'page_id' => 45,
                'action' => 'list'
            ],[
                'page_id' => 45,
                'action' => 'view'
            ],[
                'page_id' => 46,
                'action' => 'add'
            ],[
                'page_id' => 47,
                'action' => 'update'
            ],[
                'page_id' => 48,
                'action' => 'update'
            ],[
                'page_id' => 49,
                'action' => 'update'
            ],[
                'page_id' => 50,
                'action' => 'list'
            ],[
                'page_id' => 50,
                'action' => 'view'
            ],[
                'page_id' => 50,
                'action' => 'update'
            ],[
                'page_id' => 51,
                'action' => 'add'
            ],[
                'page_id' => 52,
                'action' => 'list'
            ],[
                'page_id' => 52,
                'action' => 'add'
            ],[
                'page_id' => 52,
                'action' => 'view'
            ],[
                'page_id' => 52,
                'action' => 'update'
            ],[
                'page_id' => 53,
                'action' => 'list'
            ],[
                'page_id' => 53,
                'action' => 'add'
            ],[
                'page_id' => 53,
                'action' => 'view'
            ],[
                'page_id' => 53,
                'action' => 'update'
            ],[
                'page_id' => 54,
                'action' => 'add'
            ],[
                'page_id' => 55,
                'action' => 'list'
            ],[
                'page_id' => 55,
                'action' => 'view'
            ],[
                'page_id' => 56,
                'action' => 'add'
            ],[
                'page_id' => 55,
                'action' => 'delete'
            ],

        ];
        PageActions::insert($data);
    }
}
