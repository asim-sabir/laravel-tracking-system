<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'first_name' => 'info',
            'last_name' => '',
            'email' => 'info@connexis.com',
            'password' => bcrypt('123456'),
            'level' => 'level-1',
            'verified' => 1,
            'status' => 1
        ];

        $user = User::create($data);

        $user->assignRole('Admin');
    }
}
