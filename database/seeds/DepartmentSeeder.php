<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'dept_name' => 'Sale'
            ],
            [
                'dept_name' => 'CRM'
            ]
        ];

        Department::insert($data);
    }
}
