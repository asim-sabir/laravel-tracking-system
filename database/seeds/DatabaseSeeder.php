<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(PageActionsSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(\Database\Seeder\Cities\PakistanSeeder::class);
    }
}
