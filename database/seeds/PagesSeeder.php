<?php

use Illuminate\Database\Seeder;
use App\Models\Pages;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'page_name' => 'Company',
                'page_slug' => 'company',
                'level' => 'level-1',
                'department' => '',
            ], [
                'page_name' => 'Product',
                'page_slug' => 'product',
                'level' => 'level-1',
                'department' => ''
            ], [
                'page_name' => 'Feature',
                'page_slug' => 'feature',
                'level' => 'level-1',
                'department' => ''
            ], [
                'page_name' => 'Package',
                'page_slug' => 'package',
                'level' => 'level-1',
                'department' => ''
            ], [
                'page_name' => 'User',
                'page_slug' => 'user',
                'level' => 'level-1',
                'department' => ''
            ], [
                'page_name' => 'Role',
                'page_slug' => 'role',
                'level' => 'level-1',
                'department' => ''
            ],
            // level - 2
            /**-- sale --*/
            [
                'page_name' => 'User profile',
                'page_slug' => 'level-2_hod_sale_user_profile',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Index',
                'page_slug' => 'level-2_hod_sale_index',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Client',
                'page_slug' => 'level-2_hod_sale_client',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Client Followup chat',
                'page_slug' => 'level-2_hod_sale_client_followup',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Proposal',
                'page_slug' => 'level-2_hod_sale_proposal',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Lead',
                'page_slug' => 'level-2_hod_sale_lead',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Lead Followup chat',
                'page_slug' => 'level-2_hod_sale_lead_followup',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Sale',
                'page_slug' => 'level-2_hod_sale_sale',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Report',
                'page_slug' => 'level-2_hod_sale_report',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Agent',
                'page_slug' => 'level-2_hod_sale_agent',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Ticket',
                'page_slug' => 'level-2_hod_sale_ticket',
                'level' => 'level-2',
                'department' => '1'
            ], [
                'page_name' => 'Ticket Followup chat',
                'page_slug' => 'level-2_hod_sale_ticket_followup',
                'level' => 'level-2',
                'department' => '1'
            ],
            /**-- crm --*/
            [
                'page_name' => 'User profile',
                'page_slug' => 'level-2_hod_crm_user_profile',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Index',
                'page_slug' => 'level-2_hod_crm_index',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Client',
                'page_slug' => 'level-2_hod_crm_client',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Client Followup chat',
                'page_slug' => 'level-2_hod_crm_client_followup',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Approved ticket Client',
                'page_slug' => 'level-2_hod_crm_approved_client',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Approved ticket emergency contact',
                'page_slug' => 'level-2_hod_crm_approved_emergency_contact',
                'level' => 'level-2',
                'department' => '2'
            ],[
                'page_name' => 'Approved ticket vehicle',
                'page_slug' => 'level-2_hod_crm_approved_vehicle',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Lead',
                'page_slug' => 'level-2_hod_crm_lead',
                'level' => 'level-2',
                'department' => '2'
            ],  [
                'page_name' => 'Lead Followup chat',
                'page_slug' => 'level-2_hod_crm_lead_followup',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Ticket',
                'page_slug' => 'level-2_hod_crm_ticket',
                'level' => 'level-2',
                'department' => '2'
            ],[
                'page_name' => 'Operational Ticket',
                'page_slug' => 'level-2_hod_crm_operational_ticket',
                'level' => 'level-2',
                'department' => '2'
            ], [
                'page_name' => 'Ticket Followup chat',
                'page_slug' => 'level-2_hod_crm_ticket_followup',
                'level' => 'level-2',
                'department' => '2'
            ],
            // level - 3
            /**-- sale --*/
            [
                'page_name' => 'User profile',
                'page_slug' => 'level-3_sale_user_profile',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Index',
                'page_slug' => 'level-3_sale_index',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Client',
                'page_slug' => 'level-3_sale_client',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Client Followup chat',
                'page_slug' => 'level-3_sale_client_followup',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Proposal',
                'page_slug' => 'level-3_sale_proposal',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Lead',
                'page_slug' => 'level-3_sale_lead',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Lead Followup chat',
                'page_slug' => 'level-3_sale_lead_followup',
                'level' => 'level-3',
                'department' => '1'
            ],[
                'page_name' => 'Sale',
                'page_slug' => 'level-3_sale_sale',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Report',
                'page_slug' => 'level-3_sale_report',
                'level' => 'level-3',
                'department' => '1'
            ],[
                'page_name' => 'Ticket',
                'page_slug' => 'level-3_sale_ticket',
                'level' => 'level-3',
                'department' => '1'
            ],[
                'page_name' => 'Operational Ticket',
                'page_slug' => 'level-3_sale_operational_ticket',
                'level' => 'level-3',
                'department' => '1'
            ], [
                'page_name' => 'Ticket Followup chat',
                'page_slug' => 'level-3_sale_ticket_followup',
                'level' => 'level-3',
                'department' => '1'
            ],
            /**-- CRM --*/
            [
                'page_name' => 'User profile',
                'page_slug' => 'level-3_crm_user_profile',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Index',
                'page_slug' => 'level-3_crm_index',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Client',
                'page_slug' => 'level-3_crm_client',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Client Followup chat',
                'page_slug' => 'level-3_crm_client_followup',
                'level' => 'level-3',
                'department' => '2'
            ],  [
                'page_name' => 'Approved ticket Client',
                'page_slug' => 'level-3_crm_approved_client',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Approved ticket emergency contact',
                'page_slug' => 'level-3_crm_approved_emergency_contact',
                'level' => 'level-3',
                'department' => '2'
            ],[
                'page_name' => 'Approved ticket vehicle',
                'page_slug' => 'level-3_crm_approved_vehicle',
                'level' => 'level-3',
                'department' => '2'
            ],[
                'page_name' => 'Lead',
                'page_slug' => 'level-3_crm_lead',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Lead Followup chat',
                'page_slug' => 'level-3_crm_lead_followup',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Ticket',
                'page_slug' => 'level-3_crm_ticket',
                'level' => 'level-3',
                'department' => '2'
            ],[
                'page_name' => 'Operational Ticket',
                'page_slug' => 'level-3_crm_operational_ticket',
                'level' => 'level-3',
                'department' => '2'
            ], [
                'page_name' => 'Ticket Followup chat',
                'page_slug' => 'level-3_crm_ticket_followup',
                'level' => 'level-3',
                'department' => '2'
            ],
            //level 2 crm agent
            [
                'page_name' => 'Agent',
                'page_slug' => 'level-2_hod_crm_agent',
                'level' => 'level-2',
                'department' => '2'
            ],
            [
                'page_name' => 'Assign Lead',
                'page_slug' => 'level-2_hod_crm_lead_assign_agent',
                'level' => 'level-2',
                'department' => '2'
            ]
        ];
        Pages::insert($data);
    }
}
