<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_leads', function (Blueprint $table) {
            $table->string('lead_id', 50);
            $table->string('proposal_id', 50);
            $table->integer('user_id');
            $table->tinyInteger('status')->default(0)
                ->comment('0:in progress 1:approval request 2: approved 3: revision 4: rejected 5: cancel');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_leads');
    }
}
