<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_stock', function (Blueprint $table) {
            $table->increments('stock_id');
            $table->integer('product_id');
            $table->integer('user_id');
            $table->string('unit_serial_no')->nullable();
            $table->string('product_imei')->nullable();
            $table->string('gsm_no')->nullable();
            $table->string('quantity')->nullable();
            $table->string('cost_price');
            $table->string('sale_price');
            $table->string('mini_sale_price');
            $table->tinyInteger('status')->default(1)->comment('0:inactive 1:active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_stock');
    }
}
