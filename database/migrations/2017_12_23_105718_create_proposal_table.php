<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_proposal', function (Blueprint $table) {
            $table->string('proposal_id', 20)->primary();
            $table->string('customer_id', 20);
            $table->integer('user_id');
            $table->string('name');
            $table->decimal('proposal_sale_price',10);
            $table->decimal('proposal_discount', 10)->nullable();
            $table->longText('description')->nullable();
            $table->string('pdf_file_path')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0:in progress 1:lead generated');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_proposal');
    }
}
