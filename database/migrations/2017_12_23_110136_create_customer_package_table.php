<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_package', function (Blueprint $table) {
            $table->increments('customer_package_id');
            $table->string('proposal_id');
            $table->integer('package_id');
            $table->string('product_name')->nullable();
            $table->string('package_name');
            $table->decimal('package_cost', 10);
            $table->decimal('package_sale_price', 10);
            $table->decimal('package_min_cost', 10);
            $table->decimal('package_discount', 10)->nullable();
            $table->string('package_duration')->nullable();
            $table->longText('package_description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_package');
    }
}
