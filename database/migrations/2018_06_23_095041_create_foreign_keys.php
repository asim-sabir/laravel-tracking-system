<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');

        Schema::table($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames){
            $table->foreign('permission_id')->references('id')->on($tableNames['permissions'])->onDelete('cascade');
        });

        Schema::table($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames){
            $table->foreign('role_id')->references('id')->on($tableNames['roles'])->onDelete('cascade');
        });

        Schema::table($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames){
            $table->foreign('permission_id')->references('id')->on($tableNames['permissions'])->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on($tableNames['roles'])->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        Schema::table($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames){
            $table->dropForeign(['permission_id']);
        });

        Schema::table($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames){
            $table->dropForeign(['role_id']);
        });

        Schema::table($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames){
            $table->dropForeign(['permission_id']);
            $table->dropForeign(['role_id']);
        });
    }
}
