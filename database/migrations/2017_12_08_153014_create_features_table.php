<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_features', function (Blueprint $table) {
            $table->increments('feature_id');
            $table->integer('user_id');
            $table->string('feature_name');
            $table->decimal('feature_sale_price', 10);
            $table->decimal('feature_cost_price', 10);
            $table->longText('feature_description')->nullable();
            $table->tinyInteger('feature_type')->default(1)->comment('1:hardware, 0: software');
            $table->tinyInteger('status')->default(1)->comment('0:inactive 1:active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_features');
    }
}
