<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_customer_vehicle_lead_history', function (Blueprint $table){
            $table->foreign('vehicle_id')->references('vehicle_id')->on('tb_customer_vehicle')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_customer_vehicle_lead_history', function (Blueprint $table){
            $table->dropForeign(['vehicle_id']);
        });
    }
}
