<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_vehicle', function (Blueprint $table) {
            $table->string('vehicle_id', 50)->primary();
            $table->string('customer_id', 50);
            $table->string('plate_no')->nullable();
            $table->string('year_of_manufacturing')->nullable();
            $table->string('vehicle_make')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('vehicle_engine_cc')->nullable();
            $table->string('vehicle_engine_no')->nullable();
            $table->string('vehicle_chassis_no')->nullable();
            $table->tinyInteger('fuel_type')->default(0)->comment('0:Diesel 1:Petrol 2:CNG');
            $table->tinyInteger('transmission_type')->default(0)->comment('0:Manual 1:Auto');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_vehicle');
    }
}
