<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerVehicleLeadHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_vehicle_lead_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle_id', 50);
            $table->string('lead_id', 50);
            $table->tinyInteger('status')->default(1)->comment('0: lead deactivate 1:active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_vehicle_lead_history');
    }
}
