<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tickets', function (Blueprint $table) {
            $table->string('ticket_id', 50)->primary();
            $table->integer('created_by');
            $table->string('customer_id', 50);
            $table->integer('dept_id')->nullable();
            $table->string('ticket_title');
            $table->longText('description')->nullable();
            $table->tinyInteger('priority_level')->defautl(0)->comment('0: Normal 1:Minor 2: Major 3: Critical');
            $table->tinyInteger('status')->defautl(0)->comment('0: open 1:closed');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tickets');
    }
}
