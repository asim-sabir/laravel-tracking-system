<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_packages', function (Blueprint $table) {
            $table->increments('package_id');
            $table->integer('product_id');
            $table->integer('user_id');
            $table->string('package_name');
            $table->decimal('package_cost', 10);
            $table->decimal('package_sale_price', 10);
            $table->decimal('package_min_cost', 10);
            $table->decimal('package_discount', 10)->nullable();
            $table->string('package_duration')->nullable();
            $table->longText('package_description')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0:inactive 1:active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_packages');
    }
}
