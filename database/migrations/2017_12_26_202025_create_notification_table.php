<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->integer('dept_id');
            $table->string('job_post');
            $table->string('message');
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('is_seen')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_notifications');
    }
}
