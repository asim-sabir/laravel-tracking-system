<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationalTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_operational_ticket', function (Blueprint $table) {
            $table->string('operational_ticket_id', 50)->primary();
            $table->integer('created_by');
            $table->integer('dept_id');
            $table->string('vehicle_id', 50)->nullable();
            $table->string('customer_id', 50);
            $table->string('ticket_title');
            $table->longText('description')->nullable();
            $table->tinyInteger('type')->default(0)
                ->comment('0: edit client basic info 1:e contact edit 2:edit vehicle info 3: change of vehicle 4: Remove/De install 5:Change of confidential info 6: Change of plan');
            $table->tinyInteger('priority_level')->defautl(0)->comment('0: Normal 1:Minor 2: Major 3: Critical');
            $table->tinyInteger('status')->defautl(0)->comment('0: open 1:assigned 2: reject 3 close');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_operational_ticket');
    }
}
