<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyContactPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_emergency_contact_person', function (Blueprint $table) {
            $table->increments('contact_id');
            $table->string('customer_id');
            $table->string('contact_person_name')->nullable();
            $table->string('contact_relation')->nullable();
            $table->string('mobile_no', 50)->nullable();
            $table->string('phone_no', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_emergency_contact_person');
    }
}
