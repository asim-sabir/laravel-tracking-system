<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer', function (Blueprint $table) {
            $table->string('customer_id', 20)->primary();
            $table->integer('user_id');
            $table->string('social_title')->nullable();
            $table->string('customer_name');
            $table->string('id_card_no')->nullable();
            $table->string('ntn_no')->nullable();
            $table->dateTime('date_of_birthday')->nullable();
            $table->string('designation')->nullable();
            $table->string('company_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_1')->nullable();
            $table->string('mobile_2')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('address_line_1')->nullable();
            $table->string('address_line_2')->nullable();
            $table->tinyInteger('customer_type')->default(0)->comment('0:Individual 1:Corporate');
            $table->tinyInteger('verified')->default(0)->comment('0:not verified 1:verified');
            $table->tinyInteger('status')->default(0)->comment('0:inactive 1:active');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer');
    }
}
