<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerFollowUpHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_followup_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id', 20);
            $table->integer('user_id');
            $table->longText('comment');
            $table->tinyInteger('commented_by')->default(0)->comment('0: self 1: customer');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_followup_history');
    }
}
