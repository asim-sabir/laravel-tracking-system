<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketFollowupHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_ticket_followup_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket_id');
            $table->integer('user_id');
            $table->longText('comment');
            $table->tinyInteger('commented_by')->default(0)->comment('0: self 1: other');
            $table->tinyInteger('is_edit')->default(0)->comment('0: not 1: yes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_ticket_followup_history');
    }
}
