<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPackageFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_package_features', function (Blueprint $table) {
            $table->increments('customer_package_feature_id');
            $table->integer('customer_package_id');
            $table->string('proposal_id');
            $table->integer('feature_id');
            $table->string('feature_name');
            $table->decimal('feature_sale_price', 10);
            $table->tinyInteger('is_free')->default(0)->comment('0:not free 1:free');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_package_features');
    }
}
