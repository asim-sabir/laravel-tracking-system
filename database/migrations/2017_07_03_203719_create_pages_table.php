<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pages', function (Blueprint $table){
            $table->increments('page_id');
            $table->string('page_name');
            $table->string('page_route_name')->nullable();
            $table->string('page_slug')->nullable();
            $table->string('level')->nullable();
            $table->string('department')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pages');
    }
}
